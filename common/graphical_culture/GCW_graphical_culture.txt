# Graphical culture is connected to the looks used for ships and cities
# Setting fallback will allow the game to try and use another culture if the asset is missing
# Culture is scripted for species classes, see "common/species_classes/" and "graphical_culture = xxx"
# gfxculture avian_01

imperial_01 = {
	fallback = mammalian_01
	ship_lighting = {
		cam_light_1_dir = { 0.5 0.0 0.5 }
		cam_light_2_dir = { -0.5 0.2 0.0 }
		cam_light_3_dir = { 0.5 -1.0 0.0 }
		
		intensity_near = 0.6
		intensity_far = 0.6
		near_value = 700
		far_value = 700
		rim_start_near = 0.5
		rim_stop_near = 0.99
		rim_start_far = 0.4
		rim_stop_far = 0.99
		#rim_start_far = 1.5
		#rim_stop_far = 2.0
		ambient_near = 0.1
		ambient_far = 1.0
	}
}

rebel_01 = {
	fallback = mammalian_01
	ship_lighting = {
		cam_light_1_dir = { 0.5 0.0 0.5 }
		cam_light_2_dir = { -0.5 0.2 0.0 }
		cam_light_3_dir = { 0.5 -1.0 0.0 }
		
		intensity_near = 0.6
		intensity_far = 0.6
		near_value = 700
		far_value = 700
		rim_start_near = 0.5
		rim_stop_near = 0.99
		rim_start_far = 0.4
		rim_stop_far = 0.99
		#rim_start_far = 1.5
		#rim_stop_far = 2.0
		ambient_near = 0.1
		ambient_far = 1.0
	}
}

hapes_01 = {
	fallback = mammalian_01
	ship_lighting = {
		cam_light_1_dir = { 0.5 0.0 0.5 }
		cam_light_2_dir = { -0.5 0.2 0.0 }
		cam_light_3_dir = { 0.5 -1.0 0.0 }
		
		intensity_near = 0.6
		intensity_far = 0.6
		near_value = 700
		far_value = 700
		rim_start_near = 0.5
		rim_stop_near = 0.99
		rim_start_far = 0.4
		rim_stop_far = 0.99
		#rim_start_far = 1.5
		#rim_stop_far = 2.0
		ambient_near = 0.1
		ambient_far = 1.0
	}
}

hutt_01 = {
	fallback = mammalian_01
	ship_lighting = {
		cam_light_1_dir = { 0.5 0.0 0.5 }
		cam_light_2_dir = { -0.5 0.2 0.0 }
		cam_light_3_dir = { 0.5 -1.0 0.0 }
		
		intensity_near = 0.6
		intensity_far = 0.6
		near_value = 700
		far_value = 700
		rim_start_near = 0.5
		rim_stop_near = 0.99
		rim_start_far = 0.4
		rim_stop_far = 0.99
		#rim_start_far = 1.5
		#rim_stop_far = 2.0
		ambient_near = 0.1
		ambient_far = 1.0
	}
}

mando_01 = {
	fallback = mammalian_01
	ship_lighting = {
		cam_light_1_dir = { 0.5 0.0 0.5 }
		cam_light_2_dir = { -0.5 0.2 0.0 }
		cam_light_3_dir = { 0.5 -1.0 0.0 }
		
		intensity_near = 0.6
		intensity_far = 0.6
		near_value = 700
		far_value = 700
		rim_start_near = 0.5
		rim_stop_near = 0.99
		rim_start_far = 0.4
		rim_stop_far = 0.99
		#rim_start_far = 1.5
		#rim_stop_far = 2.0
		ambient_near = 0.1
		ambient_far = 1.0
	}
}

csa_01 = {
	fallback = mammalian_01
	ship_lighting = {
		cam_light_1_dir = { 0.5 0.0 0.5 }
		cam_light_2_dir = { -0.5 0.2 0.0 }
		cam_light_3_dir = { 0.5 -1.0 0.0 }
		
		intensity_near = 0.6
		intensity_far = 0.6
		near_value = 700
		far_value = 700
		rim_start_near = 0.5
		rim_stop_near = 0.99
		rim_start_far = 0.4
		rim_stop_far = 0.99
		#rim_start_far = 1.5
		#rim_stop_far = 2.0
		ambient_near = 0.1
		ambient_far = 1.0
	}
}

cis_01 = {
	fallback = mammalian_01
	ship_lighting = {
		cam_light_1_dir = { 0.5 0.0 0.5 }
		cam_light_2_dir = { -0.5 0.2 0.0 }
		cam_light_3_dir = { 0.5 -1.0 0.0 }
		
		intensity_near = 0.6
		intensity_far = 0.6
		near_value = 700
		far_value = 700
		rim_start_near = 0.5
		rim_stop_near = 0.99
		rim_start_far = 0.4
		rim_stop_far = 0.99
		#rim_start_far = 1.5
		#rim_stop_far = 2.0
		ambient_near = 0.1
		ambient_far = 1.0
	}
}

corellian_01 = {
	fallback = mammalian_01
	ship_lighting = {
		cam_light_1_dir = { 0.5 0.0 0.5 }
		cam_light_2_dir = { -0.5 0.2 0.0 }
		cam_light_3_dir = { 0.5 -1.0 0.0 }
		
		intensity_near = 0.6
		intensity_far = 0.6
		near_value = 700
		far_value = 700
		rim_start_near = 0.5
		rim_stop_near = 0.99
		rim_start_far = 0.4
		rim_stop_far = 0.99
		#rim_start_far = 1.5
		#rim_stop_far = 2.0
		ambient_near = 0.1
		ambient_far = 1.0
	}
}

misc_01 = {
	fallback = mammalian_01
	ship_lighting = {
		cam_light_1_dir = { 0.5 0.0 0.5 }
		cam_light_2_dir = { -0.5 0.2 0.0 }
		cam_light_3_dir = { 0.5 -1.0 0.0 }
		
		intensity_near = 0.6
		intensity_far = 0.6
		near_value = 700
		far_value = 700
		rim_start_near = 0.5
		rim_stop_near = 0.99
		rim_start_far = 0.4
		rim_stop_far = 0.99
		#rim_start_far = 1.5
		#rim_stop_far = 2.0
		ambient_near = 0.1
		ambient_far = 1.0
	}
}

misc_02 = {
	fallback = mammalian_01
	ship_lighting = {
		cam_light_1_dir = { 0.5 0.0 0.5 }
		cam_light_2_dir = { -0.5 0.2 0.0 }
		cam_light_3_dir = { 0.5 -1.0 0.0 }
		
		intensity_near = 0.6
		intensity_far = 0.6
		near_value = 700
		far_value = 700
		rim_start_near = 0.5
		rim_stop_near = 0.99
		rim_start_far = 0.4
		rim_stop_far = 0.99
		#rim_start_far = 1.5
		#rim_stop_far = 2.0
		ambient_near = 0.1
		ambient_far = 1.0
	}
}

misc_03 = {
	fallback = mammalian_01
	ship_lighting = {
		cam_light_1_dir = { 0.5 0.0 0.5 }
		cam_light_2_dir = { -0.5 0.2 0.0 }
		cam_light_3_dir = { 0.5 -1.0 0.0 }
		
		intensity_near = 0.6
		intensity_far = 0.6
		near_value = 700
		far_value = 700
		rim_start_near = 0.5
		rim_stop_near = 0.99
		rim_start_far = 0.4
		rim_stop_far = 0.99
		#rim_start_far = 1.5
		#rim_stop_far = 2.0
		ambient_near = 0.1
		ambient_far = 1.0
	}
}