@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

togrutan_spawn_initializer = {
	name = "Shili"
	class = "sc_k"
	usage = custom_empire
	init_effect = { log = "togrutan homeworld" }
	asteroids_distance = 200
	max_instances = 1
	flags = { togrutan_homeworld }
	planet = { name = "Shili Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	change_orbit = 200
	planet = { count = { min = 2 max = 4 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = { 
		name = "Cafreutune" 
		class = pc_molten 
		orbit_distance = -150
		size = 9 
		has_ring = no 
	}
	planet = { 
		name = "Leskuna" 
		class = pc_toxic 
		orbit_distance = 50
		size = 9 
		has_ring = no 
	}
	planet = {
		name = "Shili"
		class = pc_continental
		orbit_distance = 50
		orbit_angle = 20
		size = { min = 21 max = 25 }
		starting_planet = yes
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = togrutan_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = togrutan_dominion } } }
				create_species = { 
					name = "Togruta" 
					class = TOG 
					portrait = togruta 
					homeworld = THIS 
					traits = {
						trait = "trait_weak"
						trait = "trait_sedentary"
						trait = "trait_conservational"
						trait = "trait_natural_sociologists"
						trait = "trait_adaptive"
						ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = togpop }
				create_country = {
					name = TogrutanDominion
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_free_haven" civic = "civic_environmentalist" }
					authority = auth_democratic
					name_list = "HUM3"
					ethos = { ethic = "ethic_xenophile" ethic = "ethic_fanatic_spiritualist" }
					species = event_target:togpop
					flag = random
				}
				last_created_country = {
					set_country_flag = togrutan_dominion
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = togrutan_dominion
					set_graphical_culture = misc_01
					give_technology = { tech = "tech_rebel_snub_1" message = no }
					#give_technology = { tech = "" message = no }
					change_country_flag = {
						icon = { category = "pointy" file = "flag_pointy_21.dds" }
						background = { category = "backgrounds" file = "v.dds" }
						colors = { "dark_green" "dark_green" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = togrutan_dominion }
				save_global_event_target_as = togrutan_dominion
				give_technology = { tech = "tech_rebel_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = togpop }
			}
			random_country = {
				limit = { has_country_flag = togrutan_dominion }
				save_event_target_as = togrutan_dominion
			}
			set_owner = event_target:togrutan_dominion
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:togrutan_dominion }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:togrutan_dominion }					
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }		
					create_pop = { species = event_target:togrutan_dominion }		
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:togrutan_dominion }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:togrutan_dominion }	
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }		
				create_pop = { species = event_target:togrutan_dominion }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:togrutan_dominion }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 1 replace = yes }	
				create_pop = { species = event_target:togrutan_dominion }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 2 replace = yes }	
				create_pop = { species = event_target:togrutan_dominion }					
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }		
				create_pop = { species = event_target:togrutan_dominion }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:togrutan_dominion }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:togrutan_dominion }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:togrutan_dominion }					
			}
		}
		moon = { class = random_non_colonizable size = 5 orbit_distance = 9 orbit_angle = { min = 80 max = 100 } has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 2 orbit_angle = { min = 80 max = 100 } has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 orbit_angle = { min = 80 max = 100 } has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 2 orbit_angle = { min = 80 max = 100 } has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 orbit_angle = { min = 80 max = 100 } has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 orbit_angle = { min = 80 max = 100 } has_ring = no }
	}
	planet = {
		name = "Ieliv"
		class = random_non_colonizable
		orbit_distance = 110
		size = { min = 19 max = 22 }
		has_ring = no
		moon = { class = random_non_colonizable size = 4 orbit_distance = 7 has_ring = no }
	}
	planet = {
		name = "Drupoclite"
		class = pc_gas_giant
		orbit_distance = 61
		size = { min = 22 max = 25 }
		has_ring = no
		moon = { class = random_non_colonizable size = 6 orbit_distance = 7 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
	}
	planet = {
		name = "Utrurn"
		class = pc_frozen
		orbit_distance = 35
		size = { min = 8 max = 15 }
		has_ring = yes
	}
}