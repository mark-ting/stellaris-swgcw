@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

tradefed_spawn_initializer = {
	name = "Neimodia"
	class = "sc_m"
	usage = custom_empire
	init_effect = { log = "tradefed homeworld" }
	asteroids_distance = 200
	max_instances = 1
	flags = { tradefed_homeworld }
	planet = { name = "Nen" class = star orbit_distance = 0 orbit_angle = 1 size = 50 has_ring = no }
	change_orbit = 200
	planet = { count = { min = 5 max = 7 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = { name = "Deel" class = random_non_colonizable orbit_distance = -147 size = 13 has_ring = no }
	planet = {
		name = "Moonan"
		class = "pc_toxic"
		orbit_distance = 53
		size = 14
		has_ring = no
		moon = { class = random_non_colonizable size = 7 orbit_distance = 13 has_ring = no }
		moon = { class = random_non_colonizable size = 8 orbit_distance = 3 has_ring = no }
	}
	planet = {
		name = "Neimodia"
		class = pc_continental
		orbit_distance = 42
		orbit_angle = 90
		size = { min = 18 max = 23 }
		starting_planet = yes
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = tradefed_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = trade_federation } } }
				create_species = { name = "Neimodian" class = NEI portrait = neimodian homeworld = THIS traits = { trait = "trait_weak" trait = "trait_thrifty" trait = "trait_conservational" ideal_planet_class = "pc_continental" } }
				last_created_species = { save_global_event_target_as = tfpop }
				create_country = {
					name = TradeFederation
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_cutthroat_politics" civic = "civic_mining_guilds" }
					authority = auth_oligarchic
					name_list = "REP3"
					ethos = { ethic = "ethic_authoritarian" ethic = "ethic_fanatic_materialist" }
					species = event_target:tfpop
					flag = random
				}
				last_created_country = {
					set_country_flag = trade_federation
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = trade_federation
					set_graphical_culture = cis_01
					give_technology = { tech = "tech_cis_snub_1" message = no }
					#give_technology = { tech = "" message = no }	
					change_country_flag = {
						icon = { category = "starwars" file = "tradefed_01.dds" }
						background = { category = "backgrounds" file = "v.dds" }
						colors = { "orange" "orange" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = trade_federation }
				save_global_event_target_as = trade_federation
				give_technology = { tech = "tech_cis_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = tfpop }
			}
			random_country = {
				limit = { has_country_flag = trade_federation }
				save_event_target_as = trade_federation
			}
			set_owner = event_target:trade_federation
			if = {
				limit = { NOT = { exists = event_target:tfmuun } }
				create_species = {
					name = "Muun"
					plural = "Muun"
					class = "MUN"
					portrait = "muun"
					name_list="HUM4"
					traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_continental" }
				}
				last_created_species = { save_global_event_target_as = tfmuun }
			}
			if = {
				limit = { NOT = { exists = event_target:tfhuman } }
				create_species = {
					name = "Human"
					plural = "Humans"
					class = "HUM"
					portrait = "human"
					name_list="HUM4"
					traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_continental" }
				}
				last_created_species = { save_global_event_target_as = tfhuman }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:trade_federation }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:tfmuun }					
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }		
					create_pop = { species = event_target:tfhuman }		
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:trade_federation }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:tfhuman }	
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }		
				create_pop = { species = event_target:tfmuun }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:trade_federation }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 1 replace = yes }	
				create_pop = { species = event_target:tfmuun }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 2 replace = yes }	
				create_pop = { species = event_target:tfmuun }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 1 replace = yes }	
				create_pop = { species = event_target:trade_federation }					
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }		
				create_pop = { species = event_target:tfhuman }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:trade_federation }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:tfhuman }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = energy amount = 1 replace = yes }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = minerals amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = food amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = food amount = 1 replace = yes }				
			}
		}
		moon = { name = "Garbage Moon" class = "pc_toxic" size = 4 orbit_distance = 10 has_ring = no }
	}
	planet = {
		name = "Partainu"
		class = "pc_gas_giant"
		orbit_distance = 123
		size = 24
		has_ring = no
		moon = { class = random_non_colonizable size = 7 orbit_distance = 7 has_ring = no }
		moon = { class = random_non_colonizable size = 6 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 4 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 4 has_ring = no }
	}
}

catoneimodia_system_initializer = {
	name = "Cato Neimodia"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "catoneimodia system" }
	planet = { name = "Cato Neimodia Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	planet = {
		name = "Mustea"
		class = random_non_colonizable
		orbit_distance = 100
		size = 9
		has_ring = no
	}
	planet = {
		name = "Beibos"
		class = random_non_colonizable
		orbit_distance = 50
		size = 13
		has_ring = no
		moon = { class = random_non_colonizable size = 5 orbit_distance = 8 orbit_angle = 24 has_ring = no }
	}
	planet = {
		name = "Cato Neimodia"
		class = "pc_continental"
		orbit_distance = 50
		orbit_angle = 50
		size = { min = 17 max = 23 }
		tile_blockers = none
		modifiers = none
		has_ring = no		
		init_effect = {
			if = {
				limit = { exists = event_target:trade_federation }
				set_owner = event_target:trade_federation
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:trade_federation ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 2 replace = yes }	
						create_pop = { species = event_target:tfhuman ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:tfhuman ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:trade_federation ethos = owner }
					}	
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 2 replace = yes }		
						create_pop = { species = event_target:tfhuman }					
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:trade_federation }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:trade_federation }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:trade_federation }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:trade_federation }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:tfhuman }	
				}
			}
		}
		moon = { class = random_non_colonizable size = 5 orbit_distance = 9 orbit_angle = 24 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 orbit_angle = 24 has_ring = no }
	}	
	planet = {
		name = "Vegradus"
		class = "pc_gas_giant"
		orbit_distance = 50
		size = 35
		has_ring = no
		moon = { class = random_non_colonizable size = 5 orbit_distance = 12 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 0 has_ring = no }
	}
}

vulpter_system_initializer = {
	name = "Vulpter"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "vulpter system" }
	planet = { name = "Vulpter Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	planet = {
		name = "Vulpus"
		class = random_non_colonizable
		orbit_distance = 75
		size = 9
		has_ring = no
	}
	planet = {
		name = "Kins"
		class = random_non_colonizable
		orbit_distance = 50
		size = { min = 10 max = 15 }
		has_ring = no
		moon = { class = random_non_colonizable size = 5 orbit_distance = 8 orbit_angle = 24 has_ring = no }
	}
	planet = {
		name = "Vulpter"
		class = "pc_arctic"
		orbit_distance = 50
		orbit_angle = 100
		size = { min = 21 max = 25 }
		tile_blockers = none
		modifiers = none
		has_ring = yes
		init_effect = {
			if = {
				limit = { exists = event_target:trade_federation }
				set_owner = event_target:trade_federation
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:trade_federation ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 2 replace = yes }	
						create_pop = { species = event_target:tfhuman ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:tfhuman ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:trade_federation ethos = owner }
					}	
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 2 replace = yes }		
						create_pop = { species = event_target:trade_federation }					
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:trade_federation }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:trade_federation }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:trade_federation }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:trade_federation }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:tfhuman }	
				}
			}
		}
	}
	planet = {
		name = "Nur"
		class = random_non_colonizable
		orbit_distance = 50
		size = { min = 8 max = 13 }
		has_ring = no
	}
	planet = {
		name = "Vulp Minor"
		class = random_non_colonizable
		orbit_distance = 50
		size = { min = 10 max = 15 }
		has_ring = no
	}
	planet = {
		name = "Vulp Major"
		class = random_non_colonizable
		orbit_distance = 50
		size = { min = 13 max = 18 }
		has_ring = no
	}
}

sarapin_system_initializer = {
	name = "Sarapin"
	class = "sc_k"
	usage = custom_empire
	init_effect = { log = "sarapin system" }
	planet = { name = "Sarapin Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	planet = {
		name = "Sarapin"
		class = "pc_desert"
		orbit_distance = 50
		orbit_angle = 20
		size = { min = 15 max = 18 }
		tile_blockers = none
		modifiers = none
		has_ring = yes
		init_effect = {
			if = {
				limit = { exists = event_target:trade_federation }
				set_owner = event_target:trade_federation
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:trade_federation ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 2 replace = yes }	
						create_pop = { species = event_target:tfhuman ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:tfhuman ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:trade_federation ethos = owner }
					}	
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 2 replace = yes }		
						create_pop = { species = event_target:trade_federation }					
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:trade_federation }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:tfhuman }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:trade_federation }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:trade_federation }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:tfhuman }	
				}
			}
		}
	}
	planet = {
		name = "Sarapin II"
		class = random_non_colonizable
		orbit_distance = 50
		size = { min = 10 max = 15 }
		has_ring = no
		moon = { class = random_non_colonizable size = 5 orbit_distance = 8 orbit_angle = 24 has_ring = no }
	}
	planet = {
		name = "Sarapin III"
		class = random_non_colonizable
		orbit_distance = 50
		size = { min = 10 max = 15 }
		has_ring = no
	}
	planet = {
		name = "Sarapin IV"
		class = "pc_continental"
		orbit_distance = 50
		orbit_angle = 100
		size = { min = 21 max = 25 }
		tile_blockers = none
		modifiers = none
		has_ring = yes
		init_effect = {
			if = {
				limit = { exists = event_target:trade_federation }
				set_owner = event_target:trade_federation
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:trade_federation ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 2 replace = yes }	
						create_pop = { species = event_target:tfhuman ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:tfhuman ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:trade_federation ethos = owner }
					}	
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 2 replace = yes }		
						create_pop = { species = event_target:trade_federation }					
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:tfhuman }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:trade_federation }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:trade_federation }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:trade_federation }
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:trade_federation }	
				}
			}
		}
	}
	planet = {
		name = "Sarapin V"
		class = "pc_gas_giant"
		orbit_distance = 50
		size = { min = 30 max = 35 }
		has_ring = no
		moon = { class = random_non_colonizable size = 5 orbit_distance = 13 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 0 has_ring = no }
	}
	planet = {
		name = "Sarapin VI"
		class = random_non_colonizable
		orbit_distance = 50
		size = { min = 10 max = 15 }
		has_ring = no
	}
	planet = {
		name = "Sarapin VII"
		class = random_non_colonizable
		orbit_distance = 50
		size = { min = 13 max = 18 }
		has_ring = no
	}
}