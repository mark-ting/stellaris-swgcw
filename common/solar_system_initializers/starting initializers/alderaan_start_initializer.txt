@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

alderaan_spawn_initializer = {
	name = "Alderaan"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "alderaan homeworld" }
	asteroids_distance = 300
	max_instances = 1
	flags = { alderaan_homeworld }
	planet = { name = "Nen" class = star orbit_distance = 0 orbit_angle = 1 size = 50 has_ring = no }
	change_orbit = 300
	planet = { count = { min = 5 max = 7 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = { name = "Raisa" class = "pc_molten" orbit_distance = -247 size = 13 has_ring = no }
	planet = {
		name = "Alderaan"
		class = pc_continental
		orbit_distance = 52
		orbit_angle = 100
		size = { min = 23 max = 25 }
		starting_planet = yes
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = alderaan_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = kingdom_of_alderaan } } }
				create_species = { name = "Human" class = ALD portrait = human homeworld = THIS traits = { trait = "trait_deviants" trait = "trait_agrarian" trait = "trait_charismatic" ideal_planet_class = "pc_continental" } }
				last_created_species = { save_global_event_target_as = aldpop }
				create_country = {
					name = KingdomOfAlderaan
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_agrarian_idyll" civic = "civic_environmentalist" }
					authority = auth_dictatorial
					name_list = "HUM3"
					ethos = { ethic = "ethic_pacifist" ethic = "ethic_fanatic_xenophile" }
					species = event_target:aldpop
					flag = random
				}
				last_created_country = {
					set_country_flag = kingdom_of_alderaan
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = kingdom_of_alderaan
					set_graphical_culture = misc_01
					give_technology = { tech = "tech_rebel_snub_1" message = no }
					#give_technology = { tech = "" message = no }	
					change_country_flag = {
						icon = { category = "starwars" file = "alderaan_01.dds" }
						background = { category = "backgrounds" file = "v.dds" }
						colors = { "dark_teal" "dark_teal" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = kingdom_of_alderaan }
				save_global_event_target_as = kingdom_of_alderaan
				give_technology = { tech = "tech_rebel_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = aldpop }
			}
			random_country = {
				limit = { has_country_flag = kingdom_of_alderaan }
				save_event_target_as = kingdom_of_alderaan
			}
			set_owner = event_target:kingdom_of_alderaan
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:kingdom_of_alderaan }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:kingdom_of_alderaan }					
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }		
					create_pop = { species = event_target:kingdom_of_alderaan }		
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:kingdom_of_alderaan }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:kingdom_of_alderaan }	
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }		
				create_pop = { species = event_target:kingdom_of_alderaan }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:kingdom_of_alderaan }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 1 replace = yes }	
				create_pop = { species = event_target:kingdom_of_alderaan }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 2 replace = yes }	
				create_pop = { species = event_target:kingdom_of_alderaan }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 1 replace = yes }	
				create_pop = { species = event_target:kingdom_of_alderaan }					
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }		
				create_pop = { species = event_target:kingdom_of_alderaan }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:kingdom_of_alderaan }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:kingdom_of_alderaan }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:kingdom_of_alderaan }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:kingdom_of_alderaan }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = food amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = food amount = 2 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = minerals amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = minerals amount = 2 replace = yes }				
			}
		}
		moon = { name = "Moon of Alderaan" class = "pc_barren" size = 5 orbit_distance = 10 has_ring = no }
	}
	planet = {
		name = "Delaya"
		class = "pc_continental"
		orbit_distance = 13
		orbit_angle = 10
		size = { min = 11 max = 13 }
		tile_blockers = none
		has_ring = no	
		init_effect = {
			if = {
				limit = { exists = event_target:kingdom_of_alderaan }
				set_owner = event_target:kingdom_of_alderaan
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:kingdom_of_alderaan ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 2 replace = yes }	
						create_pop = { species = event_target:kingdom_of_alderaan ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:kingdom_of_alderaan ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:kingdom_of_alderaan ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"	
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:kingdom_of_alderaan ethos = owner }	
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:kingdom_of_alderaan }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:kingdom_of_alderaan }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:kingdom_of_alderaan ethos = owner }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_blocker = "tb_failing_infrastructure"
					add_resource = { resource = food amount = 2 replace = yes }				
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_blocker = "tb_failing_infrastructure"
					add_resource = { resource = energy amount = 1 replace = yes }				
				}
			}
		}
	}
	planet = {
		name = "Avirandel"
		class = "pc_barren"
		orbit_distance = 74
		size = 9
		has_ring = no
		moon = { class = random_non_colonizable size = 7 orbit_distance = 7 has_ring = no }
	}
	planet = {
		name = "Ayishan"
		class = "pc_frozen"
		orbit_distance = 61
		size = 14
		has_ring = no
	}
}