@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

senex_spawn_initializer = {
	name = "Neelanon"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "senex homeworld" }
	asteroids_distance = 150
	max_instances = 1
	flags = { senex_homeworld }
	planet = { name = "Neelanon Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 50 has_ring = no }
	change_orbit = 150
	planet = { count = { min = 3 max = 6 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = { name = "Feapra" class = random_non_colonizable orbit_distance = -100 size = 13 has_ring = no }
	planet = { name = "Vusnore" class = random_non_colonizable orbit_distance = 25 size = 13 has_ring = no }
	planet = {
		name = "Oytov"
		class = random_non_colonizable
		orbit_distance = 25
		size = { min = 11 max = 13 }
		has_ring = no
		moon = { class = random_non_colonizable size = 9 orbit_distance = 10 has_ring = no }
		moon = { class = random_non_colonizable size = 6 orbit_distance = 5 has_ring = no }
	}
	planet = {
		name = "Neelanon"
		class = pc_continental
		orbit_distance = 25
		orbit_angle = 30
		size = { min = 22 max = 25 }
		starting_planet = yes
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = senex_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = senex_sector } } }
				create_species = { 
					name = "Human" 
					class = SEN 
					portrait = human 
					homeworld = THIS 
					traits = {
						trait = "trait_deviants"
						trait = "trait_conservational"
						trait = "trait_communal"
						ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = senpop }
				create_country = {
					name = SenexSector
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_shadow_council" civic = "civic_aristocratic_elite" }
					authority = auth_oligarchic
					name_list = "HUM3"
					ethos = { ethic = "ethic_egalitarian" ethic = "ethic_fanatic_xenophile" }
					species = event_target:senpop
					flag = random
				}
				last_created_country = {
					set_country_flag = senex_sector
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = senex_sector
					set_graphical_culture = misc_03
					give_technology = { tech = "tech_rebel_snub_1" message = no }
					#give_technology = { tech = "" message = no }	
					change_country_flag = {
						icon = { category = "pointy" file = "flag_pointy_14.dds" }
						background = { category = "backgrounds" file = "v.dds" }
						colors = { "beige" "beige" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = senex_sector }
				save_global_event_target_as = senex_sector
				give_technology = { tech = "tech_rebel_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = senpop }
			}
			random_country = {
				limit = { has_country_flag = senex_sector }
				save_event_target_as = senex_sector
			}
			set_owner = event_target:senex_sector
			if = {
				limit = { NOT = { exists = event_target:sentwilek } }
				create_species = {
					name = "Twi'lek"
					plural = "Twi'leks"
					class = "TWI"
					portrait = "twilek"
					name_list="HUM3"
					traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_arid" }
				}
				last_created_species = { save_global_event_target_as = sentwilek }
			}
			event_target:senex_sector = { event_target:sentwilek = { set_citizenship_type = { country = event_target:senex_sector type = citizenship_full } } }
			if = {
				limit = { NOT = { exists = event_target:sensullustan } }
				create_species = {
					name = "Sullustan"
					plural = "Sullustans"
					class = "SUL"
					portrait = "sullustan"
					name_list="HUM3"
					traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_desert" }
				}
				last_created_species = { save_global_event_target_as = sensullustan }
			}
			event_target:senex_sector = { event_target:sensullustan = { set_citizenship_type = { country = event_target:senex_sector type = citizenship_full } } }
			if = {
				limit = { NOT = { exists = event_target:senrodian } }
				create_species = {
					name = "Rodian"
					plural = "Rodians"
					class = "ROD"
					portrait = "rodian"
					name_list="HUM3"
					traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_tropical" }
				}
				last_created_species = { save_global_event_target_as = senrodian }
			}
			event_target:senex_sector = { event_target:senrodian = { set_citizenship_type = { country = event_target:senex_sector type = citizenship_full } } }
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:senex_sector }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:senex_sector }					
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }		
					create_pop = { species = event_target:senrodian }		
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:senex_sector }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:senex_sector }	
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }		
				create_pop = { species = event_target:sensullustan }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:senex_sector }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 1 replace = yes }	
				create_pop = { species = event_target:sentwilek }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 2 replace = yes }	
				create_pop = { species = event_target:senex_sector }					
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }		
				create_pop = { species = event_target:sentwilek }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:senex_sector }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:senex_sector }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:sensullustan }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = minerals amount = 2 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = food amount = 2 replace = yes }		
			}
		}
		moon = { class = random_non_colonizable size = 10 orbit_distance = 10 has_ring = no }
	}
	planet = {
		name = "Voyliv"
		class = random_non_colonizable
		orbit_distance = 55
		size = { min = 11 max = 13 }
		has_ring = no
	}
	planet = {
		name = "Iesworth"
		class = random_non_colonizable
		orbit_distance = 54
		size = { min = 10 max = 11 }
		has_ring = no
		moon = { class = random_non_colonizable size = 8 orbit_distance = 7 has_ring = no }
	}
	planet = {
		name = "Truoter"
		class = random_non_colonizable
		orbit_distance = 61
		size = { min = 8 max = 11 }
		has_ring = no
	}
	planet = {
		name = "Fourilia"
		class = random_non_colonizable
		orbit_distance = 51
		size = { min = 8 max = 11 }
		has_ring = no
	}
}

belsavis_system_initializer = {
	name = "Belsavis"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "belsavis system" }
	planet = { name = "Belsavis Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	planet = {
		name = "Greshan 86"
		class = random_non_colonizable
		orbit_distance = 50
		size = 9
		has_ring = no 
	}
	planet = {
		name = "Wualea"
		class = random_non_colonizable
		orbit_distance = 50
		size = 15
		has_ring = no 
	}
	planet = {
		name = "Belsavis"
		class = "pc_arctic"
		orbit_distance = 50
		orbit_angle = 30
		size = { min = 18 max = 23 }
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			if = {
				limit = { exists = event_target:senex_sector }
				add_modifier = { modifier = "belsavis_habitats" days = -1 }
				set_owner = event_target:senex_sector
				if = {
					limit = { NOT = { exists = event_target:senithorian } }
					create_species = {
						name = "Ithorian"
						plural = "Ithorians"
						class = "ITH"
						portrait = "ithorian"
						name_list="REP2"
						traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_tropical" }
					}
					last_created_species = { save_global_event_target_as = senithorian }
				}
				event_target:senex_sector = { event_target:senithorian = { set_citizenship_type = { country = event_target:senex_sector type = citizenship_full } } }
				if = {
					limit = { NOT = { exists = event_target:senduro } }
					create_species = {
						name = "Duro"
						plural = "Duro"
						class = "DUR"
						portrait = "duro"
						name_list="REP2"
						traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_desert" }
					}
					last_created_species = { save_global_event_target_as = senduro }
				}
				event_target:senex_sector = { event_target:senduro = { set_citizenship_type = { country = event_target:senex_sector type = citizenship_full } } }
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }	
					create_pop = { species = event_target:senex_sector }
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:senduro }					
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:senithorian }		
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:senex_sector }	
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:senduro }	
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:senex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:senithorian }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }	
					create_pop = { species = event_target:senduro }					
				}	
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:senex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:senex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:senduro }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }	
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:senex_sector }					
				}
			}
		}
	}
	planet = {
		name = "Eshao"
		class = random_non_colonizable
		orbit_distance = 50
		size = 14
		has_ring = no
	}
	planet = {
		name = "Yastrides"
		class = random_non_colonizable
		orbit_distance = 50
		size = 13
		has_ring = no
	}	
	planet = {
		name = "Muscoywei"
		class = random_non_colonizable
		orbit_distance = 50
		size = 11
		has_ring = no
	}	
}

karfeddion_system_initializer = {
	name = "Karfeddion"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "karfeddion system" }
	planet = { name = "Karfeddion Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	planet = {
		name = "Iotrapus"
		class = random_non_colonizable
		orbit_distance = 50
		size = 11
		has_ring = no 
	}
	planet = {
		name = "Feglerth"
		class = random_non_colonizable
		orbit_distance = 50
		size = 14
		has_ring = no 
	}
	planet = {
		name = "Karfeddion"
		class = "pc_continental"
		orbit_distance = 50
		orbit_angle = 100
		size = { min = 18 max = 23 }
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			if = {
				limit = { exists = event_target:senex_sector }
				set_owner = event_target:senex_sector
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }	
					create_pop = { species = event_target:senex_sector }
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:senex_sector }					
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:senex_sector }		
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:senex_sector }	
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:senex_sector }	
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:senex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:senex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }	
					create_pop = { species = event_target:senex_sector }					
				}	
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:senex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:senex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:senex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }	
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:senex_sector }					
				}
			}
		}
	}
	planet = {
		name = "Rathiathea"
		class = "pc_gas_giant"
		orbit_distance = 50
		size = 32
		has_ring = no
		moon = { class = random_non_colonizable size = 5 orbit_distance = 16 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 2 orbit_distance = 0 has_ring = no }
	}	
	planet = {
		name = "Osmaonope"
		class = random_non_colonizable
		orbit_distance = 50
		size = 9
		has_ring = no
	}	
}

senex_system_initializer = {
	name = "Senex"
	class = "sc_a"
	usage = custom_empire
	init_effect = { log = "senex system" }
	planet = { name = "Senex Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	planet = {
		name = "Airilia"
		class = random_non_colonizable
		orbit_distance = 50
		size = 10
		has_ring = no 
	}
	planet = {
		name = "Faypra"
		class = random_non_colonizable
		orbit_distance = 25
		size = 13
		has_ring = no 
	}
	planet = {
		name = "Pratelara"
		class = random_non_colonizable
		orbit_distance = 25
		size = 15
		has_ring = no 
	}
	planet = {
		name = "Senex"
		class = "pc_continental"
		orbit_distance = 50
		orbit_angle = 100
		size = { min = 18 max = 23 }
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			if = {
				limit = { exists = event_target:senex_sector }
				set_owner = event_target:senex_sector
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }	
					create_pop = { species = event_target:senex_sector }
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:senex_sector }					
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:senex_sector }		
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:senex_sector }	
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:senex_sector }	
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:senex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:senex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 2 replace = yes }	
					create_pop = { species = event_target:senex_sector }					
				}	
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:senex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:senex_sector }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:senex_sector }					
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }	
					set_building = "building_basic_science_lab_1"
					create_pop = { species = event_target:senex_sector }					
				}
			}
		}
	}
	planet = {
		name = "Broxonov"
		class = "pc_gas_giant"
		orbit_distance = 50
		size = 32
		has_ring = no
		moon = { class = random_non_colonizable size = 5 orbit_distance = 16 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 0 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 2 orbit_distance = 0 has_ring = no }
	}	
	planet = {
		name = "Slilia 89"
		class = random_non_colonizable
		orbit_distance = 50
		size = 9
		has_ring = no
	}	
}