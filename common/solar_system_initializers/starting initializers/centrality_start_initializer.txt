@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

centrality_spawn_initializer = {
	name = "Erilnar"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "centrality homeworld" }
	max_instances = 1
	flags = { centrality_homeworld }
	planet = { name = "Erilnar Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }
	planet = { count = { min = 2 max = 4 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = { 
		name = "Edruephus" 
		class = pc_molten 
		orbit_distance = 40
		size = 9 
		has_ring = no 
	}
	planet = { 
		name = "Daclite" 
		class = pc_toxic 
		orbit_distance = 50
		size = 9 
		has_ring = no 
	}
	planet = {
		name = "Erilnar"
		class = pc_continental
		orbit_distance = 50
		orbit_angle = 20
		size = { min = 21 max = 25 }
		starting_planet = yes
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = centrality_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = the_centrality } } }
				create_species = { 
					name = "Human" 
					class = CEN 
					portrait = human 
					homeworld = THIS 
					traits = {
						trait = "trait_deviants"
						trait = "trait_adaptive"
						trait = "trait_conservational"
						ideal_planet_class = "pc_continental" 
					} 
				}
				last_created_species = { save_global_event_target_as = cenpop }
				create_country = {
					name = TheCentrality
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_beacon_of_liberty" civic = "civic_idealistic_foundation" }
					authority = auth_democratic
					name_list = "HUM3"
					ethos = { ethic = "ethic_egalitarian" ethic = "ethic_xenophile" ethic = "ethic_pacifist" }
					species = event_target:cenpop
					flag = random
				}
				last_created_country = {
					set_country_flag = the_centrality
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = the_centrality
					set_graphical_culture = misc_03
					give_technology = { tech = "tech_rebel_snub_1" message = no }
					#give_technology = { tech = "" message = no }
					change_country_flag = {
						icon = { category = "starwars" file = "centrality_01.dds" }
						background = { category = "backgrounds" file = "v.dds" }
						colors = { "purple" "purple" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = the_centrality }
				save_global_event_target_as = the_centrality
				give_technology = { tech = "tech_rebel_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = cenpop }
			}
			random_country = {
				limit = { has_country_flag = the_centrality }
				save_event_target_as = the_centrality
			}
			set_owner = event_target:the_centrality
			if = {
				limit = { NOT = { exists = event_target:centwilek } }
				create_species = {
					name = "Twi'lek"
					plural = "Twi'leks"
					class = "TWI"
					portrait = "twilek"
					name_list="HUM3"
					traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_continental" }
				}
				last_created_species = { save_global_event_target_as = centwilek }
			}
			event_target:the_centrality = { event_target:centwilek = { set_citizenship_type = { country = event_target:the_centrality type = citizenship_limited } } }
			if = {
				limit = { NOT = { exists = event_target:cenbothan } }
				create_species = {
					name = "Bothan"
					plural = "Bothans"
					class = "BOT"
					portrait = "bothan"
					name_list="HUM4"
					traits = { trait = "trait_adaptive" trait = "trait_nomadic" ideal_planet_class = "pc_continental" }
				}
				last_created_species = { save_global_event_target_as = cenbothan }
			}
			event_target:the_centrality = { event_target:cenbothan = { set_citizenship_type = { country = event_target:the_centrality type = citizenship_limited } } }
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:the_centrality }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:the_centrality }					
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }		
					create_pop = { species = event_target:centwilek }		
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:the_centrality }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:the_centrality }	
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }		
				create_pop = { species = event_target:cenbothan }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:centwilek }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 1 replace = yes }	
				create_pop = { species = event_target:centwilek }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 2 replace = yes }	
				create_pop = { species = event_target:the_centrality }					
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }		
				create_pop = { species = event_target:the_centrality }					
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }		
				create_pop = { species = event_target:cenbothan }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:centwilek }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:the_centrality }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:the_centrality }					
			}
		}
	}
	planet = {
		name = "Skakuphus"
		class = pc_gas_giant
		orbit_distance = 60
		size = { min = 19 max = 22 }
		has_ring = yes
		moon = { class = random_non_colonizable size = 4 orbit_distance = 7 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 5 orbit_distance = 3 has_ring = no }
	}
	planet = {
		name = "Chofathea"
		class = pc_gas_giant
		orbit_distance = 61
		size = { min = 22 max = 25 }
		has_ring = no
		moon = { class = random_non_colonizable size = 6 orbit_distance = 7 has_ring = no }
	}
	planet = {
		name = "Juynope"
		class = random_non_colonizable
		orbit_distance = 42
		size = { min = 8 max = 15 }
		has_ring = yes
	}
}

oseon_system_initializer = {
	name = "Oseon"
	class = "sc_g"
	asteroids_distance = 200
	usage = custom_empire
	init_effect = { log = "oseon system" }
	planet = { name = "Oseon" class = star orbit_distance = 0 orbit_angle = 1 size = 50 has_ring = no }
	change_orbit = 200
	planet = { count = { min = 2 max = 4 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = {
		name = "Oseon I"
		class = random_non_colonizable
		orbit_distance = -175
		size = 9
		has_ring = no
	}
	planet = {
		name = "Oseon II"
		class = random_non_colonizable
		orbit_distance = 25
		size = 14
		has_ring = no
	}
	planet = {
		name = "Oseon III"
		class = random_non_colonizable
		orbit_distance = 25
		size = 16
		has_ring = no
	}
	planet = {
		name = "Oseon IV"
		class = random_non_colonizable
		orbit_distance = 25
		size = 13
		has_ring = no
	}
	planet = {
		name = "Oseon V"
		class = random_non_colonizable
		orbit_distance = 25
		size = 15
		has_ring = no
	}
	planet = {
		name = "Oseon VI"
		class = random_non_colonizable
		orbit_distance = 25
		size = 12
		has_ring = no
	}
	planet = {
		name = "Oseon VII"
		class = "pc_continental"
		orbit_distance = 25
		orbit_angle = 100
		size = { min = 15 max = 20 }
		tile_blockers = none
		modifiers = none
		has_ring = no		
		init_effect = {
			if = {
				limit = { exists = event_target:the_centrality }
				set_owner = event_target:the_centrality
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 2 replace = yes }		
					create_pop = { species = event_target:the_centrality ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 2 replace = yes }	
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:the_centrality ethos = owner }
					}	
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 2 replace = yes }		
						create_pop = { species = event_target:centwilek }					
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"	
					add_resource = { resource = energy amount = 1 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"	
					add_resource = { resource = food amount = 1 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
			}
		}
	}
}

renatasia_system_initializer = {
	name = "Renatasia"
	class = "sc_g"
	asteroids_distance = 250
	usage = custom_empire
	init_effect = { log = "renatasia system" }
	planet = { name = "Renatasia" class = star orbit_distance = 0 orbit_angle = 1 size = 50 has_ring = no }
	change_orbit = 250
	planet = { count = { min = 2 max = 4 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = {
		name = "Renatasia I"
		class = random_non_colonizable
		orbit_distance = -200
		size = 9
		has_ring = no
	}
	planet = {
		name = "Renatasia II"
		class = random_non_colonizable
		orbit_distance = 50
		size = 14
		has_ring = no
	}
	planet = {
		name = "Renatasia III"
		class = "pc_continental"
		orbit_distance = 50
		orbit_angle = 100
		size = { min = 15 max = 20 }
		tile_blockers = none
		modifiers = none
		has_ring = no		
		init_effect = {
			if = {
				limit = { exists = event_target:the_centrality }
				set_owner = event_target:the_centrality
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:the_centrality ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 2 replace = yes }	
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 2 replace = yes }		
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:the_centrality ethos = owner }
					}	
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:centwilek }					
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"	
					add_resource = { resource = energy amount = 1 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"	
					add_resource = { resource = food amount = 3 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
			}
		}
	}
	planet = {
		name = "Renatasia IV"
		class = "pc_continental"
		orbit_distance = 50
		orbit_angle = 50
		size = { min = 11 max = 14 }
		tile_blockers = none
		modifiers = none
		has_ring = no		
		init_effect = {
			if = {
				limit = { exists = event_target:the_centrality }
				set_owner = event_target:the_centrality
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 2 replace = yes }		
					create_pop = { species = event_target:the_centrality ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 2 replace = yes }	
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:the_centrality ethos = owner }
					}	
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 2 replace = yes }		
						create_pop = { species = event_target:centwilek }					
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"	
					add_resource = { resource = energy amount = 1 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"	
					add_resource = { resource = food amount = 1 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 2 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
			}
		}
	}
	planet = {
		name = "Renatasia V"
		class = "pc_gas_giant"
		orbit_distance = 100
		size = 32
		has_ring = yes
	}
}

tund_system_initializer = {
	name = "Tund"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "tund system" }
	planet = { name = "Tund Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 50 has_ring = no }
	planet = {
		name = "Vagloewei"
		class = random_non_colonizable
		orbit_distance = 50
		size = 9
		has_ring = no
	}
	planet = {
		name = "Utruylia"
		class = random_non_colonizable
		orbit_distance = 50
		size = 14
		has_ring = no
	}
	planet = {
		name = "Tund"
		class = "pc_arid"
		orbit_distance = 50
		orbit_angle = 50
		size = { min = 17 max = 22 }
		tile_blockers = none
		modifiers = none
		has_ring = no		
		init_effect = {
			if = {
				limit = { exists = event_target:the_centrality }
				set_owner = event_target:the_centrality
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:the_centrality ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:the_centrality ethos = owner }
					}	
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:centwilek }					
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"	
					add_resource = { resource = energy amount = 1 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"	
					add_resource = { resource = food amount = 1 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
			}
		}
	}
	planet = {
		name = "Naflara"
		class = "pc_gas_giant"
		orbit_distance = 50
		size = 35
		has_ring = yes
	}
	planet = {
		name = "Naflara"
		class = "pc_gas_giant"
		orbit_distance = 50
		size = 31
		has_ring = no
	}
}

antipose_system_initializer = {
	name = "Antipose"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "antipose system" }
	planet = { name = "Antipose" class = star orbit_distance = 0 orbit_angle = 1 size = 50 has_ring = no }
	planet = {
		name = "Antipose I"
		class = random_non_colonizable
		orbit_distance = 25
		size = 9
		has_ring = no
	}
	planet = {
		name = "Antipose II"
		class = random_non_colonizable
		orbit_distance = 25
		size = 11
		has_ring = no
	}
	planet = {
		name = "Antipose III"
		class = random_non_colonizable
		orbit_distance = 25
		size = 11
		has_ring = no
	}
	planet = {
		name = "Antipose IV"
		class = random_non_colonizable
		orbit_distance = 25
		size = 13
		has_ring = no
	}
	planet = {
		name = "Antipose V"
		class = random_non_colonizable
		orbit_distance = 25
		size = 14
		has_ring = no
	}
	planet = {
		name = "Antipose VI"
		class = random_non_colonizable
		orbit_distance = 25
		size = 15
		has_ring = no
	}
	planet = {
		name = "Antipose VII"
		class = random_non_colonizable
		orbit_distance = 25
		size = 15
		has_ring = no
	}
	planet = {
		name = "Antipose VIII"
		class = random_non_colonizable
		orbit_distance = 25
		size = 16
		has_ring = no
	}
	planet = {
		name = "Antipose IX"
		class = "pc_continental"
		orbit_distance = 25
		orbit_angle = 100
		size = { min = 15 max = 20 }
		tile_blockers = none
		modifiers = none
		has_ring = no		
		init_effect = {
			if = {
				limit = { exists = event_target:the_centrality }
				set_owner = event_target:the_centrality
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:the_centrality ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:the_centrality ethos = owner }
					}	
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:centwilek }					
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"	
					add_resource = { resource = energy amount = 1 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"	
					add_resource = { resource = food amount = 1 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = society_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = physics_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
			}
		}
	}
	planet = {
		name = "Antipose X"
		class = random_non_colonizable
		orbit_distance = 25
		size = 15
		has_ring = no
	}
	planet = {
		name = "Antipose XI"
		class = random_non_colonizable
		orbit_distance = 25
		size = 16
		has_ring = no
	}
	planet = {
		name = "Antipose XII"
		class = "pc_continental"
		orbit_distance = 25
		orbit_angle = 100
		size = { min = 12 max = 16 }
		tile_blockers = none
		modifiers = none
		has_ring = no		
		init_effect = {
			if = {
				limit = { exists = event_target:the_centrality }
				set_owner = event_target:the_centrality
				random_tile = {
					limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
					set_building = "building_capital_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:the_centrality ethos = owner }		
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }	
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_hydroponics_farm_1"
						add_resource = { resource = food amount = 1 replace = yes }		
						create_pop = { species = event_target:centwilek ethos = owner }
					}
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_power_plant_1"
						add_resource = { resource = energy amount = 1 replace = yes }	
						create_pop = { species = event_target:the_centrality ethos = owner }
					}	
					random_neighboring_tile = {
						limit = { has_blocker = no has_building = no }
						set_building = "building_mining_network_1"
						add_resource = { resource = minerals amount = 1 replace = yes }		
						create_pop = { species = event_target:centwilek }					
					}
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"	
					add_resource = { resource = energy amount = 1 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"	
					add_resource = { resource = food amount = 1 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:the_centrality }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"	
					add_resource = { resource = minerals amount = 1 replace = yes }
					create_pop = { species = event_target:centwilek }	
				}
				random_tile = {
					limit = { has_blocker = no has_building = no }
					add_resource = { resource = engineering_research amount = 1 replace = yes }
					set_building = "building_basic_science_lab_1"	
					create_pop = { species = event_target:the_centrality }	
				}
			}
		}
	}
	planet = {
		name = "Antipose XIII"
		class = "pc_gas_giant"
		orbit_distance = 50
		size = 35
		has_ring = yes
	}
}