@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

taris_spawn_initializer = {
	name = "Taris"
	class = "sc_k"
	usage = custom_empire
	init_effect = { log = "taris homeworld" }
	asteroids_distance = 270
	max_instances = 1
	flags = { taris_homeworld }
	planet = { name = "Taris Prime" class = star orbit_distance = 0 orbit_angle = 1 size = 50 has_ring = no }
	change_orbit = 270
	planet = { count = { min = 2 max = 3 } class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } }
	planet = { name = "Rogue Moon" class = random_asteroid orbit_distance = 0 orbit_angle = { min = 80 max = 100 } size = 11 }
	planet = { name = "Ostraynerth" class = random_non_colonizable orbit_distance = -227 size = 13 has_ring = no }
	planet = { name = "Mosnadus" class = random_non_colonizable orbit_distance = 23 size = 13 has_ring = no }
	planet = {
		name = "Taris"
		class = pc_continental
		orbit_distance = 34
		orbit_angle = 100
		size = { min = 23 max = 25 }
		starting_planet = yes
		has_ring = no
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = taris_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = taris_authority } } }
				create_species = { name = "Human" class = TAR portrait = human homeworld = THIS traits = { trait = "trait_sedentary" trait = "trait_communal" trait = "trait_industrious" ideal_planet_class = "pc_continental" } }
				last_created_species = { save_global_event_target_as = tarpop }
				create_country = {
					name = TarisAuthority
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_cutthroat_politics" civic = "civic_aristocratic_elite" }
					authority = auth_oligarchic
					name_list = "HUM3"
					ethos = { ethic = "ethic_xenophile" ethic = "ethic_fanatic_materialist" }
					species = event_target:tarpop
					flag = random
				}
				last_created_country = {
					set_country_flag = taris_authority
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = taris_authority
					set_graphical_culture = misc_01
					give_technology = { tech = "tech_rebel_snub_1" message = no }
					#give_technology = { tech = "" message = no }	
					change_country_flag = {
						icon = { category = "ornate" file = "flag_ornate_24.dds" }
						background = { category = "backgrounds" file = "v.dds" }
						colors = { "teal" "teal" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = taris_authority }
				save_global_event_target_as = taris_authority
				give_technology = { tech = "tech_rebel_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = tarpop }
			}
			random_country = {
				limit = { has_country_flag = taris_authority }
				save_event_target_as = taris_authority
			}
			set_owner = event_target:taris_authority
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:taris_authority }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:taris_authority }					
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }		
					create_pop = { species = event_target:taris_authority }		
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:taris_authority }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:taris_authority }	
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }		
				create_pop = { species = event_target:taris_authority }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:taris_authority }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 1 replace = yes }	
				create_pop = { species = event_target:taris_authority }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = food amount = 2 replace = yes }	
				create_pop = { species = event_target:taris_authority }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 1 replace = yes }	
				create_pop = { species = event_target:taris_authority }					
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }		
				create_pop = { species = event_target:taris_authority }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:taris_authority }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:taris_authority }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:taris_authority }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:taris_authority }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = minerals amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = energy amount = 1 replace = yes }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = energy amount = 1 replace = yes }
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = minerals amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = minerals amount = 2 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_blocker = "tb_failing_infrastructure"
				add_resource = { resource = food amount = 2 replace = yes }				
			}
		}
		moon = { class = random_non_colonizable size = 10 orbit_distance = 10 has_ring = no }
		moon = { class = random_non_colonizable size = 6 orbit_distance = 5 has_ring = no }
		moon = { class = random_non_colonizable size = 7 orbit_distance = 5 has_ring = no }
		moon = { class = random_non_colonizable size = 6 orbit_distance = 5 has_ring = no }
	}
	planet = {
		name = "Featurn"
		class = random_non_colonizable
		orbit_distance = 34
		size = { min = 11 max = 13 }
		has_ring = no
		moon = { class = random_non_colonizable size = 9 orbit_distance = 10 has_ring = no }
	}
	planet = {
		name = "Uobos"
		class = random_non_colonizable
		orbit_distance = 54
		size = { min = 10 max = 11 }
		has_ring = no
		moon = { class = random_non_colonizable size = 8 orbit_distance = 7 has_ring = yes }
	}
	planet = {
		name = "Shoewei"
		class = "pc_gas_giant"
		orbit_distance = 57
		size = { min = 22 max = 27 }
		has_ring = yes
		moon = { class = random_non_colonizable size = 6 orbit_distance = 10 has_ring = no }
		moon = { class = random_non_colonizable size = 6 orbit_distance = 5 has_ring = yes }
		moon = { class = random_non_colonizable size = 6 orbit_distance = 5 has_ring = no }
	}
}