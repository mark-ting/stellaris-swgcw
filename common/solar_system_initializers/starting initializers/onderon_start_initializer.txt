@distance = 50
@planet_min_size = 10
@planet_max_size = 25
@base_moon_distance = 10
@moon_min_size = 6

onderon_spawn_initializer = {
	name = "Japrael"
	class = "sc_g"
	usage = custom_empire
	init_effect = { log = "onderon homeworld" }
	max_instances = 1
	flags = { onderon_homeworld }
	planet = { name = "Prael" class = star orbit_distance = 0 orbit_angle = 1 size = 45 has_ring = no }	
	planet = {
		name = "Kiskua"
		class = "pc_molten"
		orbit_distance = 60
		size = 9
		has_ring = no
		moon = { class = random_non_colonizable size = 3 orbit_distance = 7 has_ring = no }
	}
	planet = {
		name = "Onderon"
		class = pc_tropical
		orbit_distance = 100
		orbit_angle = 100
		size = { min = 21 max = 25 }
		starting_planet = yes
		has_ring = yes
		tile_blockers = none
		modifiers = none
		init_effect = { prevent_anomaly = yes }
		init_effect = {
			set_global_flag = onderon_homeworld_spawned
			if = {
				limit = { NOT = { any_country = { has_country_flag = kingdom_of_onderon } } }
				create_species = { name = "Human" class = OND portrait = human homeworld = THIS traits = { trait = "trait_solitary" trait = "trait_deviants" trait = "trait_adaptive" trait = "trait_intelligent" ideal_planet_class = "pc_tropical" } }
				last_created_species = { save_global_event_target_as = ondpop }
				create_country = {
					name = KingdomOfOnderon
					type = default
					ignore_initial_colony_error = yes
					civics = { civic = "civic_agrarian_idyll" civic = "civic_aristocratic_elite" }
					authority = auth_imperial
					name_list = "HUM4"
					ethos = { ethic = "ethic_xenophile" ethic = "ethic_pacifist" ethic = "ethic_materialist" }
					species = event_target:ondpop
					flag = random
				}
				last_created_country = {
					set_country_flag = kingdom_of_onderon
					set_country_flag = custom_start_screen
					set_country_flag = gcw_empire
					save_global_event_target_as = kingdom_of_onderon
					set_graphical_culture = misc_03
					give_technology = { tech = "tech_rebel_snub_1" message = no }
					#give_technology = { tech = "" message = no }	
					change_country_flag = {
						icon = { category = "starwars" file = "onderon_01.dds" }
						background = { category = "backgrounds" file = "00_solid.dds" }
						colors = { "dark_teal" "dark_teal" "null" "null" }
					}
				}
			}
			set_capital = yes
			random_country = {
				limit = { has_country_flag = kingdom_of_onderon }
				save_global_event_target_as = kingdom_of_onderon
				give_technology = { tech = "tech_rebel_snub_1" message = no }
				#give_technology = { tech = "" message = no }	
				species = { save_global_event_target_as = ondpop }
			}
			random_country = {
				limit = { has_country_flag = kingdom_of_onderon }
				save_event_target_as = kingdom_of_onderon
			}
			set_owner = event_target:kingdom_of_onderon
			random_tile = {
				limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
				set_building = "building_capital_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:kingdom_of_onderon }
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }	
					create_pop = { species = event_target:kingdom_of_onderon }					
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_hydroponics_farm_1"
					add_resource = { resource = food amount = 1 replace = yes }		
					create_pop = { species = event_target:kingdom_of_onderon }		
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_power_plant_1"
					add_resource = { resource = energy amount = 1 replace = yes }		
					create_pop = { species = event_target:kingdom_of_onderon }	
				}
				random_neighboring_tile = {
					limit = { has_blocker = no has_building = no }
					set_building = "building_mining_network_1"
					add_resource = { resource = minerals amount = 1 replace = yes }		
					create_pop = { species = event_target:kingdom_of_onderon }	
				}
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }		
				create_pop = { species = event_target:kingdom_of_onderon }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_power_plant_1"
				add_resource = { resource = energy amount = 1 replace = yes }	
				create_pop = { species = event_target:kingdom_of_onderon }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_hydroponics_farm_1"
				add_resource = { resource = minerals amount = 2 replace = yes }	
				create_pop = { species = event_target:kingdom_of_onderon }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 1 replace = yes }	
				create_pop = { species = event_target:kingdom_of_onderon }					
			}	
			random_tile = {
				limit = { has_blocker = no has_building = no }
				set_building = "building_mining_network_1"
				add_resource = { resource = minerals amount = 2 replace = yes }		
				create_pop = { species = event_target:kingdom_of_onderon }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = engineering_research amount = 1 replace = yes }
				set_building = "building_basic_science_lab_1"	
				create_pop = { species = event_target:kingdom_of_onderon }	
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = physics_research amount = 1 replace = yes }	
				set_building = "building_basic_science_lab_1"
				create_pop = { species = event_target:kingdom_of_onderon }					
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = energy amount = 1 replace = yes }				
			}
			random_tile = {
				limit = { has_blocker = no has_building = no }
				add_resource = { resource = food amount = 1 replace = yes }				
			}
		}
		moon = {
			name = "Dxun"
			class = "pc_tropical"
			orbit_distance = 12
			orbit_angle = 20
			size = { min = 8 max = 12 }
			tile_blockers = none
			has_ring = no		
			init_effect = {
				if = {
					limit = { exists = event_target:kingdom_of_onderon }
					set_owner = event_target:kingdom_of_onderon
					random_tile = {
						limit = { has_blocker = no has_building = no num_adjacent_tiles > 3 }
						set_building = "building_capital_1"
						add_resource = { resource = energy amount = 1 replace = yes }		
						create_pop = { species = event_target:kingdom_of_onderon ethos = owner }		
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_hydroponics_farm_1"
							add_resource = { resource = food amount = 2 replace = yes }	
							create_pop = { species = event_target:kingdom_of_onderon ethos = owner }
						}
						random_neighboring_tile = {
							limit = { has_blocker = no has_building = no }
							set_building = "building_mining_network_1"	
							add_resource = { resource = minerals amount = 1 replace = yes }		
							create_pop = { species = event_target:kingdom_of_onderon ethos = owner }	
						}
					}
				}
			}
		}
		moon = { name = "Dagri" class = random_non_colonizable size = 3 orbit_distance = 3 has_ring = no }
		moon = { name = "Evas" class = random_non_colonizable size = 3 orbit_distance = 2 has_ring = no }
		moon = { name = "Suthre" class = random_non_colonizable size = 2 orbit_distance = 1 has_ring = no }
	}
	planet = {
		name = "Fillata"
		class = "pc_toxic"
		orbit_distance = 60
		size = { min = 7 max = 10 }
		tile_blockers = none
		has_ring = no
		moon = { class = random_non_colonizable size = 5 orbit_distance = 8 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 2 has_ring = no }
	}
	planet = {
		name = "Morvolo"
		class = "pc_frozen"
		orbit_distance = 70
		size = { min = 7 max = 10 }
		tile_blockers = none
		has_ring = yes
		moon = { class = random_non_colonizable size = 5 orbit_distance = 8 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
	}
	planet = {
		name = "Mulchoop"
		class = "pc_gas_giant"
		orbit_distance = 70
		size = { min = 27 max = 30 }
		tile_blockers = none
		has_ring = yes
		moon = { class = random_non_colonizable size = 5 orbit_distance = 8 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
	}
	planet = {
		name = "Caloma"
		class = "pc_gas_giant"
		orbit_distance = 70
		size = { min = 27 max = 30 }
		tile_blockers = none
		has_ring = yes
		moon = { class = random_non_colonizable size = 5 orbit_distance = 8 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
	}
	planet = {
		name = "Twing"
		class = "pc_gas_giant"
		orbit_distance = 70
		size = { min = 27 max = 30 }
		tile_blockers = none
		has_ring = yes
		moon = { class = random_non_colonizable size = 5 orbit_distance = 8 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
	}
	planet = {
		name = "Overt"
		class = "pc_gas_giant"
		orbit_distance = 70
		size = { min = 27 max = 30 }
		tile_blockers = none
		has_ring = yes
		moon = { class = random_non_colonizable size = 5 orbit_distance = 8 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 3 has_ring = no }
		moon = { class = random_non_colonizable size = 4 orbit_distance = 2 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
		moon = { class = random_non_colonizable size = 3 orbit_distance = 1 has_ring = no }
	}
}





