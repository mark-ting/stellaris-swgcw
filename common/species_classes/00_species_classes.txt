# Number of entries controlls amount to choose from in designer  I.E.   "mol1" "mol2" "mol3" "mol4"
# graphical_culture is connected to the looks used for ships and cities, see "common/graphical_culture/"
# Portraits here are one you can choose from in the character creater. For prescripted races they are grabbed straight from the .gfx file. 
# playable=(yes/no)/trigger, default is yes, if this species class can is playable
# randomized=(yes/no)/trigger, default is yes, if this species class is randomized
# custom_portraits
#	randomized = trigger, default is no, to specify if portraits are randomized
#	playable = trigger, default is yes, to specify if portraits are playable
#	portraits, list portrait keys

IMP = { randomized = no portraits = { "imperial" } graphical_culture = imperial_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "IMP" uplifted_portraits = { "imperial" } }
REB = { randomized = no portraits = { "rebel" } graphical_culture = rebel_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "REB" uplifted_portraits = { "rebel" } }

HAP = { randomized = no portraits = { "human" } graphical_culture = hapes_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "HAP" uplifted_portraits = { "human" } }
MAN = { randomized = no portraits = { "human" } graphical_culture = mando_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "MAN" uplifted_portraits = { "human" } }
CSA = { randomized = no portraits = { "human" } graphical_culture = csa_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "CSA" uplifted_portraits = { "human" } }
COR = { randomized = no portraits = { "human" } graphical_culture = corellian_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "COR" uplifted_portraits = { "human" } }
ALD = { randomized = no portraits = { "human" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "ALD" uplifted_portraits = { "human" } }
JAV = { randomized = no portraits = { "human" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "JAV" uplifted_portraits = { "human" } }
TAR = { randomized = no portraits = { "human" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "TAR" uplifted_portraits = { "human" } }
CHA = { randomized = no portraits = { "human" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "CHA" uplifted_portraits = { "human" } }
BAK = { randomized = no portraits = { "human" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "BAK" uplifted_portraits = { "human" } }
ORD = { randomized = no portraits = { "human" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "ORD" uplifted_portraits = { "human" } }
TIO = { randomized = no portraits = { "human" } graphical_culture = misc_02 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "TIO" uplifted_portraits = { "human" } }
JUV = { randomized = no portraits = { "human" } graphical_culture = misc_02 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "JUV" uplifted_portraits = { "human" } }
CEN = { randomized = no portraits = { "human" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "CEN" uplifted_portraits = { "human" } }
SEN = { randomized = no portraits = { "human" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "SEN" uplifted_portraits = { "human" } }
OND = { randomized = no portraits = { "human" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "OND" uplifted_portraits = { "human" } }
BAL = { randomized = no portraits = { "human" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "BAL" uplifted_portraits = { "human" } }

HUT = { randomized = no portraits = { "hutt" } graphical_culture = hutt_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "HUT" uplifted_portraits = { "hutt" } }
NEI = { randomized = no portraits = { "neimodian" } graphical_culture = cis_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "NEI" uplifted_portraits = { "neimodian" } }
DUR = { randomized = no portraits = { "duro" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "DUR" uplifted_portraits = { "duro" } }
ETT = { randomized = no portraits = { "etti" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "ETT" uplifted_portraits = { "etti" } }
ARK = { randomized = no portraits = { "arkanian" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "ARK" uplifted_portraits = { "arkanian" } }
ECH = { randomized = no portraits = { "echani" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "ECH" uplifted_portraits = { "echani" } }
THY = { randomized = no portraits = { "thyrsian" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "THY" uplifted_portraits = { "thyrsian" } }
CAT = { randomized = no portraits = { "cathar" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "CAT" uplifted_portraits = { "cathar" } }
RAT = { randomized = no portraits = { "rattataki" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "RAT" uplifted_portraits = { "rattataki" } }
ZAB = { randomized = no portraits = { "zabrak" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "ZAB" uplifted_portraits = { "zabrak" } }
TWI = { randomized = no portraits = { "twilek" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "TWI" uplifted_portraits = { "twilek" } }
ZEL = { randomized = no portraits = { "zeltron" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "ZEL" uplifted_portraits = { "zeltron" } }
PAN = { randomized = no portraits = { "pantoran" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "PAN" uplifted_portraits = { "pantoran" } }
ROD = { randomized = no portraits = { "rodian" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "ROD" uplifted_portraits = { "rodian" } }
KEL = { randomized = no portraits = { "keldor" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "KEL" uplifted_portraits = { "keldor" } }
BOT = { randomized = no portraits = { "bothan" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "BOT" uplifted_portraits = { "bothan" } }
TOG = { randomized = no portraits = { "togruta" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "TOG" uplifted_portraits = { "togruta" } }
MUN = { randomized = no portraits = { "muun" } graphical_culture = misc_02 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "MUN" uplifted_portraits = { "muun" } }
PAU = { randomized = no portraits = { "pauan" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "PAU" uplifted_portraits = { "pauan" } }
SUL = { randomized = no portraits = { "sullustan" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "SUL" uplifted_portraits = { "sullustan" } }
MON = { randomized = no portraits = { "moncalamari" } graphical_culture = rebel_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "MON" uplifted_portraits = { "moncalamari" } }
SKA = { randomized = no portraits = { "skakoan" } graphical_culture = misc_02 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "SKA" uplifted_portraits = { "skakoan" } }
ITH = { randomized = no portraits = { "ithorian" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "ITH" uplifted_portraits = { "ithorian" } }
WOO = { randomized = no portraits = { "wookie" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "WOO" uplifted_portraits = { "wookie" } }
TAL = { randomized = no portraits = { "talz" } graphical_culture = misc_03 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "TAL" uplifted_portraits = { "talz" } }

ZYG = { randomized = no portraits = { "mam3" } graphical_culture = misc_02 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "ZYG" uplifted_portraits = { "mam3" } }
TRA = { randomized = no portraits = { "rep9" } graphical_culture = misc_01 move_pop_sound_effect = "moving_pop_confirmation" uplifted_into = "TRA" uplifted_portraits = { "rep9" } }

HUM = {
	playable = { always = no }
	portraits = {
		"human"
		"humanoid_02"
		"humanoid_03"
		"humanoid_04"
		"humanoid_05"
	}
	
	# These should not be used for randomly generated species
	non_randomized_portraits = {
		"human"
	}
	
	graphical_culture = mammalian_01
	move_pop_sound_effect = "moving_pop_confirmation"
} 
 
MAM = {
	playable = { always = no }
	portraits = {
		"mam5"
		"mam13"
		"mam10"
		"mam14"
		"mam4"
		"mam9"
		"mam11"
		"mam6"
		"mam3"	
		"mam12"
		"mam7"
		"mam15"
		"mam2"
		"mam8"
		"mam1"
	}
	custom_portraits = {#add additional portraits when customizing species
		portraits = {
			"mam16"
		}
	}
	
	custom_portraits = {#add additional portraits when customizing species
		randomized = { 
			OR = { 
				host_has_dlc = "Creatures of the Void Portrait Pack"
				host_has_dlc = "Anniversary Portraits"
			}
		}
		playable = { 
			OR = {
				local_has_dlc = "Creatures of the Void Portrait Pack"
				local_has_dlc = "Anniversary Portraits"
			}
		}
		portraits = {
			"mam17"
		}
	}
	
	graphical_culture = mammalian_01
	move_pop_sound_effect = "moving_pop_confirmation"
}

REP = {
	playable = { always = no }
	portraits = {
		"rep2"		
		"rep10"		
		"rep14"		
		"rep3"		
		"rep7"		
		"rep12"
		"rep9"
		"rep11"
		"rep6"
		"rep13"
		"rep1"		
		"rep8"		
		"rep4"		
		"rep15"		
		"rep5"
	}
	
	custom_portraits = {#add additional portraits when customizing species
		randomized = { host_has_dlc = "Leviathans Story Pack" }
		playable = { local_has_dlc = "Leviathans Story Pack" }	
		portraits = {
			"rep16"
		}
	}
	custom_portraits = {
		randomized = { host_has_dlc = "Anniversary Portraits" }
		playable = { local_has_dlc = "Anniversary Portraits" }
		portraits = {
			"rep17"
		}
	}

	graphical_culture = reptilian_01
	move_pop_sound_effect = "reptilian_pops_move"
}

AVI = {
	playable = { always = no }
	portraits = {
		"avi1"
		"avi8"
		"avi13"		
		"avi10"		
		"avi15"
		"avi4"
		"avi7"
		"avi5"
		"avi9"
		"avi6"
		"avi11"		
		"avi3"		
		"avi14"		
		"avi2"		
		"avi12"
	}
	custom_portraits = {#add additional portraits when customizing species
		playable = { logged_in_to_pdx_account = yes }
		portraits = {
			"avi16"
		}
	}
	
	custom_portraits = {#add additional portraits when customizing species
		randomized = { 
			OR = { 
				host_has_dlc = "Creatures of the Void Portrait Pack"
				host_has_dlc = "Anniversary Portraits"
			}
		}
		playable = { 
			OR = {
				local_has_dlc = "Creatures of the Void Portrait Pack"
				local_has_dlc = "Anniversary Portraits"
			}
		}
		portraits = {
			"avi17"
		}
	}
	custom_portraits = {
		randomized = { host_has_dlc = "Anniversary Portraits" }
		playable = { local_has_dlc = "Anniversary Portraits" }
		portraits = {
			"avi18"
		}
	}

	graphical_culture = avian_01
	move_pop_sound_effect = "avian_pops_move"
}

ART = {
	playable = { always = no }
	portraits = {
		"art14"
		"art7"
		"art12"
		"art6"
		"art1"		
		"art13"
		"art8"
		"art3"		
		"art5"		
		"art9"
		"art10"
		"art11"
		"art15"
	}

	custom_portraits = {#add additional portraits when customizing species
		randomized = { host_has_dlc = "Leviathans Story Pack" }
		playable = { local_has_dlc = "Leviathans Story Pack" }	
		portraits = {
			"art18"
		}
	}	
	
	custom_portraits = {#add additional portraits when customizing species
		randomized = { host_has_dlc = "Arachnoid Portrait Pack" }
		playable = { local_has_dlc = "Arachnoid Portrait Pack" }
		portraits = {
			"art16"
		}
	}
	custom_portraits = {
		randomized = { host_has_dlc = "Anniversary Portraits" }
		playable = { local_has_dlc = "Anniversary Portraits" }
		portraits = {
			"art20"
		}
	}
	
	custom_portraits = {#add additional portraits when customizing species
		randomized = { 
			OR = { 
				host_has_dlc = "Creatures of the Void Portrait Pack"
				host_has_dlc = "Anniversary Portraits"
			}
		}
		playable = { 
			OR = {
				local_has_dlc = "Creatures of the Void Portrait Pack"
				local_has_dlc = "Anniversary Portraits"
			}
		}
		portraits = {
			"art17"
		}
	}
	
	graphical_culture = arthropoid_01
	move_pop_sound_effect = "arthopoid_pops_move"
}

MOL = {
	playable = { always = no }
	portraits = {
		"mol3"		# Rendered but not saved
		"mol7"		
		"mol13"
		"mol2"
		"mol14"		
		"mol4"
		"mol8"				
		"mol1"		
		"mol11"
		"mol12"
		"mol5"
		"mol6"
		"mol15"
	}
	
	custom_portraits = {#add additional portraits when customizing species
		randomized = { 
			OR = { 
				host_has_dlc = "Creatures of the Void Portrait Pack"
				host_has_dlc = "Anniversary Portraits"
			}
		}
		playable = { 
			OR = {
				local_has_dlc = "Creatures of the Void Portrait Pack"
				local_has_dlc = "Anniversary Portraits"
			}
		}
		portraits = {
			"mol16"
		}
	}
	
	custom_portraits = {#add additional portraits when customizing species
		randomized = { host_has_dlc = "Leviathans Story Pack" }
		playable = { local_has_dlc = "Leviathans Story Pack" }	
		portraits = {
			"mol17"
			"mol18"
		}
	}

	graphical_culture = molluscoid_01
	move_pop_sound_effect = "molluscoid_pops_move"
}

FUN = {
	playable = { always = no }
	portraits = {		
		"fun2"
		"fun4"
		"fun3"
		"fun13"
		"fun12"
		"fun6"
		"fun15"
		"fun7"
		"fun1"
		"fun9"		
		"fun11"		
		"fun8"		
		"fun14"
		"fun10"
	}

	custom_portraits = {#add additional portraits when customizing species
		randomized = { 
			OR = { 
				host_has_dlc = "Creatures of the Void Portrait Pack"
				host_has_dlc = "Anniversary Portraits"
			}
		}
		playable = { 
			OR = {
				local_has_dlc = "Creatures of the Void Portrait Pack"
				local_has_dlc = "Anniversary Portraits"
			}
		}
		portraits = {
			"fun16"
		}
	}
	
	custom_portraits = {#add additional portraits when customizing species
		randomized = { host_has_dlc = "Leviathans Story Pack" }
		playable = { local_has_dlc = "Leviathans Story Pack" }	
		portraits = {
			"fun17"
		}
	}

	graphical_culture = fungoid_01
	move_pop_sound_effect = "fungoid_pops_move"
}

PLANT = {
	playable = { always = no }
	playable = { local_has_dlc = "Plantoid"	}
	randomized = { host_has_dlc = "Plantoid" }

	portraits = {
		"pla2"
		"pla6"
		"pla1"
		"pla11"
		"pla3"
		"pla13"
		"pla7"
		"pla15"
		"pla10"
		"pla5"
		"pla9"
		"pla14"
		"pla4"
		"pla8"
		"pla12"
	}

	graphical_culture = plantoid_01
}

PRE_MAM = {

	playable = { always = no }
	randomized = no

	portraits = {
		"pre_mam6"
		"pre_mam13"
	}

	graphical_culture = mammalian_01
	move_pop_sound_effect = "moving_pop_confirmation"
	uplifted_into = "MAM"
	uplifted_portraits = 
	{
		"mam6"
		"mam13"	
	}
}

PRE_REP = {

	playable = { always = no }
	randomized = no

	portraits = {
		"pre_rep09"
		"pre_rep12"
	}

	graphical_culture = reptilian_01
	move_pop_sound_effect = "reptilian_pops_move"
	uplifted_into = "REP"
	uplifted_portraits = 
	{
		"rep9"
		"rep12"	
	}
}

PRE_AVI = {
	
	playable = { always = no }
	randomized = no

	portraits = {
		"pre_avi1"
		"pre_avi12"
	}

	graphical_culture = avian_01
	move_pop_sound_effect = "avian_pops_move"
	uplifted_into = "AVI"
	uplifted_portraits = 
	{
		"avi1"
		"avi12"	
	}	
}

PRE_ART = {
	
	playable = { always = no }
	randomized = no

	portraits = {
		"pre_art1"
		"pre_art11"
		"pre_art12"
		"pre_art13"
		"pre_art14"
		"pre_art15"
	}

	graphical_culture = arthropoid_01
	move_pop_sound_effect = "arthopoid_pops_move"
	uplifted_into = "ART"
	uplifted_portraits = {
		"art1"
		"art11"
		"art12"
		"art13"
		"art14"
		"art15"
	}
}

PRE_MOL = {
	
	playable = { always = no }
	randomized = no

	portraits = {
		"pre_mol1"
		"pre_mol3"
	}

	graphical_culture = molluscoid_01
	move_pop_sound_effect = "molluscoid_pops_move"
	uplifted_into = "MOL"
	uplifted_portraits = {
		"mol1"
		"mol3"
	}
}

PRE_FUN = {
	
	playable = { always = no }
	randomized = no

	portraits = {
		"pre_fun9"
		"pre_fun13"
	}

	graphical_culture = fungoid_01
	move_pop_sound_effect = "fungoid_pops_move"
	uplifted_into = "FUN"
	uplifted_portraits = {
		"fun9"
		"fun13"
	}
}

AI = {
	playable = { always = no }
	randomized = no

	portraits = {
		"ai1"
		"robot1"
	}
	
	graphical_culture = ai_01
	move_pop_sound_effect = "moving_pop_confirmation"
}

FALLEN = {
	playable = { always = no }
	randomized = no

	portraits = {
		"mam14"
	}
	
	graphical_culture = fallen_empire_01
	move_pop_sound_effect = "moving_pop_confirmation"
}

SWARM = {
	playable = { always = no }
	randomized = no

	portraits = {
		"swarm"
	}
	
	graphical_culture = swarm_01
	move_pop_sound_effect = "moving_pop_confirmation"
}

EXD = {
	playable = { always = no }
	randomized = no

	portraits = {
		"exd1"
		"exd2"		# No textures
		"exd3"		# No textures
	}

	graphical_culture = extra_dimensional_01
	move_pop_sound_effect = "moving_pop_confirmation"
}

ROBOT = {
	playable = { always = no }
	randomized = no
	robotic = yes
	
	portraits = {
		"robot1"
	}
	
	graphical_culture = ai_01
	move_pop_sound_effect = "robot_pops_move"
}
