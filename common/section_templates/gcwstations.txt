ship_section_template = {
	key = "XQ_PLATFORM_SECTION"
	ship_size = military_station_xq

	fits_on_slot = "north"
	fits_on_slot = "west"
	fits_on_slot = "east"
	fits_on_slot = "south"
	entity = "military_station_section_xq_entity"
	icon = "GFX_ship_part_core_mid"

	component_slot = {
		name = "MEDIUM_GUN_01"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_01" 
	}
	component_slot = {
		name = "MEDIUM_GUN_02"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_02" 
	}
	component_slot = {
		name = "SMALL_GUN_01"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_01" 
	}
	component_slot = {
		name = "SMALL_GUN_02"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_02" 
	}
	component_slot = {
		name = "SMALL_GUN_03"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_03" 
	}
	component_slot = {
		name = "SMALL_GUN_04"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_04" 
	}
	component_slot = {
		name = "PD_01"
		slot_size = point_defence
		slot_type = weapon
		locatorname = "small_gun_01" 
	}
	component_slot = {
		name = "PD_02"
		slot_size = point_defence
		slot_type = weapon
		locatorname = "small_gun_02" 
	}
	component_slot = {
		name = "STRIKE_CRAFT_01"
		slot_size = large
		slot_type = strike_craft
		locatorname = "hangarbay_01" 
	}

	small_utility_slots = 0
	medium_utility_slots = 0
	large_utility_slots = 6
	aux_utility_slots = 0
}

ship_section_template = {
	key = "G1_PLATFORM_SECTION"
	ship_size = military_station_g1

	fits_on_slot = "north"
	fits_on_slot = "west"
	fits_on_slot = "east"
	fits_on_slot = "south"
	entity = "military_station_section_g1_entity"
	icon = "GFX_ship_part_core_mid"
	
	component_slot = {
		name = "MEDIUM_GUN_01"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_01" 
	}
	component_slot = {
		name = "MEDIUM_GUN_02"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_02" 
	}
	component_slot = {
		name = "MEDIUM_GUN_03"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_03" 
	}
	component_slot = {
		name = "MEDIUM_GUN_04"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_04" 
	}
	component_slot = {
		name = "SMALL_GUN_01"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_01" 
	}
	component_slot = {
		name = "SMALL_GUN_02"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_02" 
	}
	component_slot = {
		name = "SMALL_GUN_03"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_03" 
	}
	component_slot = {
		name = "SMALL_GUN_04"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_04" 
	}
	component_slot = {
		name = "PD_01"
		slot_size = point_defence
		slot_type = weapon
		locatorname = "small_gun_01" 
	}
	component_slot = {
		name = "PD_02"
		slot_size = point_defence
		slot_type = weapon
		locatorname = "small_gun_02" 
	}
	component_slot = {
		name = "STRIKE_CRAFT_01"
		slot_size = large
		slot_type = strike_craft
		locatorname = "hangarbay_01" 
	}
	component_slot = {
		name = "STRIKE_CRAFT_02"
		slot_size = large
		slot_type = strike_craft
		locatorname = "hangarbay_02" 
	}

	small_utility_slots = 0
	medium_utility_slots = 0
	large_utility_slots = 6
	aux_utility_slots = 0
}

ship_section_template = {
	key = "G2_PLATFORM_SECTION"
	ship_size = military_station_g2

	fits_on_slot = "north"
	fits_on_slot = "west"
	fits_on_slot = "east"
	fits_on_slot = "south"
	entity = "military_station_section_g2_entity"
	icon = "GFX_ship_part_core_mid"
	
	component_slot = {
		name = "MEDIUM_GUN_01"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_01" 
	}
	component_slot = {
		name = "MEDIUM_GUN_02"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_02" 
	}
	component_slot = {
		name = "MEDIUM_GUN_03"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_03" 
	}
	component_slot = {
		name = "MEDIUM_GUN_04"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_04" 
	}
	component_slot = {
		name = "SMALL_GUN_01"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_01" 
	}
	component_slot = {
		name = "SMALL_GUN_02"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_02" 
	}
	component_slot = {
		name = "SMALL_GUN_03"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_03"
	}
	component_slot = {
		name = "SMALL_GUN_04"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_04"
	}
	component_slot = {
		name = "PD_01"
		slot_size = point_defence
		slot_type = weapon
		locatorname = "small_gun_01"
	}
	component_slot = {
		name = "PD_02"
		slot_size = point_defence
		slot_type = weapon
		locatorname = "small_gun_02"
	}
	component_slot = {
		name = "STRIKE_CRAFT_01"
		slot_size = large
		slot_type = strike_craft
		locatorname = "hangarbay_01"
	}
	component_slot = {
		name = "STRIKE_CRAFT_02"
		slot_size = large
		slot_type = strike_craft
		locatorname = "hangarbay_02"
	}

	small_utility_slots = 0
	medium_utility_slots = 0
	large_utility_slots = 6
	aux_utility_slots = 0
}

ship_section_template = {
	key = "G3_PLATFORM_SECTION"
	ship_size = military_station_g3

	fits_on_slot = "north"
	fits_on_slot = "west"
	fits_on_slot = "east"
	fits_on_slot = "south"
	entity = "military_station_section_g3_entity"
	icon = "GFX_ship_part_core_mid"
	
	component_slot = {
		name = "MEDIUM_GUN_01"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_01"
	}
	component_slot = {
		name = "MEDIUM_GUN_02"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_02"
	}
	component_slot = {
		name = "MEDIUM_GUN_03"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_03"
	}
	component_slot = {
		name = "MEDIUM_GUN_04"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_04"
	}
	component_slot = {
		name = "MEDIUM_GUN_05"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_05"
	}
	component_slot = {
		name = "MEDIUM_GUN_06"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_06"
	}
	component_slot = {
		name = "SMALL_GUN_01"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_01"
	}
	component_slot = {
		name = "SMALL_GUN_02"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_02"
	}
	component_slot = {
		name = "SMALL_GUN_03"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_03"
	}
	component_slot = {
		name = "SMALL_GUN_04"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_04"
	}
	component_slot = {
		name = "PD_01"
		slot_size = point_defence
		slot_type = weapon
		locatorname = "small_gun_01"
	}
	component_slot = {
		name = "PD_02"
		slot_size = point_defence
		slot_type = weapon
		locatorname = "small_gun_02"
	}
	component_slot = {
		name = "STRIKE_CRAFT_01"
		slot_size = large
		slot_type = strike_craft
		locatorname = "hangarbay_01"
	}
	component_slot = {
		name = "STRIKE_CRAFT_02"
		slot_size = large
		slot_type = strike_craft
		locatorname = "hangarbay_02"
	}

	small_utility_slots = 0
	medium_utility_slots = 0
	large_utility_slots = 6
	aux_utility_slots = 0
}

ship_section_template = {
	key = "FT_PLATFORM_SECTION"
	ship_size = military_station_ft

	fits_on_slot = "north"
	fits_on_slot = "west"
	fits_on_slot = "east"
	fits_on_slot = "south"
	entity = "military_station_section_ft_entity"
	icon = "GFX_ship_part_core_mid"
	
	component_slot = {
		name = "EXTRA_LARGE_01"
		slot_size = extra_large
		slot_type = weapon
		locatorname = "xl_gun_01"
	}
	component_slot = {
		name = "MEDIUM_GUN_01"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_01"
	}
	component_slot = {
		name = "MEDIUM_GUN_02"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_02" 
	}
	component_slot = {
		name = "MEDIUM_GUN_03"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_03" 
	}
	component_slot = {
		name = "MEDIUM_GUN_04"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_04" 
	}
	component_slot = {
		name = "MEDIUM_GUN_05"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_05" 
	}
	component_slot = {
		name = "MEDIUM_GUN_06"
		slot_size = medium
		slot_type = weapon
		locatorname = "large_gun_06" 
	}
	component_slot = {
		name = "SMALL_GUN_01"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_01" 
	}
	component_slot = {
		name = "SMALL_GUN_02"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_02" 
	}
	component_slot = {
		name = "SMALL_GUN_03"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_01" 
	}
	component_slot = {
		name = "SMALL_GUN_04"
		slot_size = small
		slot_type = weapon
		locatorname = "medium_gun_02" 
	}
	component_slot = {
		name = "PD_01"
		slot_size = point_defence
		slot_type = weapon
		locatorname = "small_gun_01" 
	}
	component_slot = {
		name = "PD_02"
		slot_size = point_defence
		slot_type = weapon
		locatorname = "small_gun_02" 
	}
	component_slot = {
		name = "STRIKE_CRAFT_01"
		slot_size = large
		slot_type = strike_craft
		locatorname = "hangarbay_01" 
	}
	component_slot = {
		name = "STRIKE_CRAFT_02"
		slot_size = large
		slot_type = strike_craft
		locatorname = "hangarbay_02" 
	}

	small_utility_slots = 0
	medium_utility_slots = 0
	large_utility_slots = 6
	aux_utility_slots = 0
}