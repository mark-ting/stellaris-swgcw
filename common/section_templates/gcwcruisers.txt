# CRUISER + HEAVY INTERDICTOR SECTIONS


# Ton-Falk / MC40 / Liberator #

	ship_section_template = {	
		key = "cr_carrier"	
		ship_size = gcwcruiser
		fits_on_slot = mid	
		entity = "gcwcruiser_mid1_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1	
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_03"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_03"
		}			
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Carrack / Strike #

	ship_section_template = {	
		key = "cr_gunship"	
		ship_size = gcwcruiser
		fits_on_slot = mid	
		entity = "gcwcruiser_mid2_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1	
		ai_weight = { factor = 11 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}
		component_slot = {
			name = "SMALL_GUN_05"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_05"
		}
		component_slot = {
			name = "SMALL_GUN_06"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_06"
		}
		component_slot = {
			name = "SMALL_GUN_07"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_07"
		}
		component_slot = {
			name = "SMALL_GUN_08"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_08"
		}	
		component_slot = {
			name = "PD_01"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_01"
		}	
		component_slot = {
			name = "PD_02"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_02"
		}	
		component_slot = {
			name = "PD_03"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_03"
		}	
		component_slot = {
			name = "PD_04"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_04"
		}			
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Immobilizer #

	ship_section_template = {	
		key = "hi_carrier"	
		ship_size = heavy_interdictor
		fits_on_slot = mid	
		entity = "heavy_interdictor_mid1_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_heavy_interdictor_mid1 }	
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}
		component_slot = {
			name = "SMALL_GUN_05"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_05"
		}
		component_slot = {
			name = "SMALL_GUN_06"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_06"
		}
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}			
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Modified Strike Cruiser #

	ship_section_template = {	
		key = "hi_gunship"	
		ship_size = heavy_interdictor
		fits_on_slot = mid	
		entity = "heavy_interdictor_mid2_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_heavy_interdictor_mid2 }		
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "SMALL_GUN_03"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_03"
		}
		component_slot = {
			name = "SMALL_GUN_04"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_04"
		}
		component_slot = {
			name = "SMALL_GUN_05"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_05"
		}
		component_slot = {
			name = "SMALL_GUN_06"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_06"
		}
		component_slot = {
			name = "SMALL_GUN_07"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_07"
		}
		component_slot = {
			name = "SMALL_GUN_08"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_08"
		}
		component_slot = {
			name = "PD_01"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_01"
		}	
		component_slot = {
			name = "PD_02"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_02"
		}	
		component_slot = {
			name = "PD_03"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_03"
		}	
		component_slot = {
			name = "PD_04"
			slot_size = point_defence
			slot_type = weapon
			locatorname = "small_gun_04"
		}			
						
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}
	
# Hapan Battle Dragon #

	ship_section_template = {	
		key = "hi_torpedoboat"	
		ship_size = heavy_interdictor
		fits_on_slot = mid	
		entity = "heavy_interdictor_mid3_entity"	
		icon = "GFX_ship_part_core_mid"	
		icon_frame = 1
		prerequisites = { tech_heavy_interdictor_mid3 }		
		ai_weight = { factor = 10 }	
		
		component_slot = {
			name = "MEDIUM_GUN_01"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_01"
		}	
		component_slot = {
			name = "MEDIUM_GUN_02"
			slot_size = medium
			slot_type = weapon
			locatorname = "large_gun_02"
		}	
		component_slot = {
			name = "SMALL_GUN_01"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_01"
		}	
		component_slot = {
			name = "SMALL_GUN_02"
			slot_size = small
			slot_type = weapon
			locatorname = "medium_gun_02"
		}
		component_slot = {
			name = "TORPEDO_01"
			slot_size = torpedo
			slot_type = weapon
			locatorname = "torpedo_01"
		}
		component_slot = {
			name = "TORPEDO_02"
			slot_size = torpedo
			slot_type = weapon
			locatorname = "torpedo_02"
		}
		component_slot = {
			name = "STRIKE_CRAFT_01"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_01"
		}	
		component_slot = {
			name = "STRIKE_CRAFT_02"
			slot_size = large
			slot_type = strike_craft
			locatorname = "hangarbay_02"
		}			
		
		small_utility_slots = 0	
		medium_utility_slots = 0	
		large_utility_slots = 6	
		aux_utility_slots = 0	
		cost = 140	
	}