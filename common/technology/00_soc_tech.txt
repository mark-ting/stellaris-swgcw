##################
### TECH COSTS ###
##################
#If you change any of these, use replace in files so the values are the same across all files

@tier1cost1 = 360
@tier1cost2 = 480
@tier1cost3 = 600
@tier1cost4 = 720

@tier2cost1 = 1000
@tier2cost2 = 1400
@tier2cost3 = 1800
@tier2cost4 = 2200

@tier3cost1 = 3000
@tier3cost2 = 4000
@tier3cost3 = 5000
@tier3cost4 = 6000

####################
### TECH WEIGHTS ###
####################

@tier1weight1 = 100
@tier1weight2 = 95
@tier1weight3 = 90
@tier1weight4 = 85

@tier2weight1 = 75
@tier2weight2 = 70
@tier2weight3 = 65
@tier2weight4 = 60

@tier3weight1 = 50
@tier3weight2 = 45
@tier3weight3 = 40
@tier3weight4 = 35

# ##################
# Basic army
# ##################

# ##################
# Farming
# ##################

# ##################
# Biolab and Colonization
# ##################

#Bio Lab 1
tech_biolab_1 = {
	cost = @tier1cost1
	area = society
	tier = 1
	category = { biology }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 5
			is_ai = yes
		}	
		modifier = {
			factor = 2
			years_passed > 5
		}
		modifier = {
			factor = 2
			years_passed > 10
		}		
		modifier = {
			factor = 2
			years_passed > 15
		}		
		modifier = {
			factor = 2
			years_passed > 20
		}		
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
}

#Bio Lab 2
tech_biolab_2 = {
	cost = @tier2cost2
	area = society
	tier = 2
	category = { biology }
	prerequisites = { "tech_biolab_1" "tech_colonial_centralization" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 2
			years_passed > 5
		}
		modifier = {
			factor = 2
			years_passed > 10
		}		
		modifier = {
			factor = 2
			years_passed > 15
		}		
		modifier = {
			factor = 2
			years_passed > 20
		}			
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}	
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
}

#Bio Lab 3
tech_biolab_3 = {
	cost = @tier3cost3
	area = society
	tier = 3
	category = { biology }	
	prerequisites = { "tech_biolab_2" "tech_galactic_administration" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 2
			years_passed > 25
		}
		modifier = {
			factor = 2
			years_passed > 30
		}		
		modifier = {
			factor = 2
			years_passed > 35
		}		
		modifier = {
			factor = 2
			years_passed > 40
		}	
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
}

# ##################
# Terraformation
# ##################

tech_climate_restoration = {
	cost = @tier3cost1
	area = society
	tier = 3
	category = { new_worlds }
	prerequisites = { "tech_ecological_adaptation" }
	is_reverse_engineerable = no
	weight = 0
	
	# unlocks additional terraforming-options
	
	prereqfor_desc = {
		diplo_action = {
			title = "TECH_UNLOCK_TERRAFORMING_NUKED_CHANGE_TITLE"
			desc = "TECH_UNLOCK_TERRAFORMING_NUKED_CHANGE_DESC"
		}
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_new_worlds"
			}
		}
		modifier = {
			factor = 3
			has_ascension_perk = ap_world_shaper
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_new_worlds"
			}
		}
	}
}

tech_gaia_creation = {
	cost = @tier3cost4
	area = society
	tier = 3
	is_rare = yes
	category = { new_worlds }
	prerequisites = { "tech_climate_restoration" }
	is_reverse_engineerable = no
	weight = 0
	
	# unlocks additional terraforming-options
	
	prereqfor_desc = {
		diplo_action = {
			title = "TECH_UNLOCK_TERRAFORMING_GAIA_CHANGE_TITLE"
			desc = "TECH_UNLOCK_TERRAFORMING_GAIA_CHANGE_DESC"
		}
	}
	
	weight_modifier = {
		factor = 0.1
		modifier = {
			factor = 0.33
			NOR = {
				research_leader = {
					area = society
					has_trait = "leader_trait_expertise_new_worlds"
				}
				has_ascension_perk = ap_world_shaper
			}
		}
		modifier = {
			factor = 3
			has_ascension_perk = ap_world_shaper
		}
		modifier = {
			factor = 2
			any_owned_planet = {
				is_planet_class = pc_gaia
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_new_worlds"
			}
		}
	}
}


# ##################
# Genetics
# ##################

#Epigenetic Triggers
tech_epigenetic_triggers = {
	cost = @tier1cost4
	area = society
	tier = 2
	category = { biology }
	prerequisites = { "tech_genome_mapping" }
	is_reverse_engineerable = no
	weight = 0
	
	feature_flags = {
		uplifting
	}
	
	weight_modifier = {
		modifier = {
			factor = 0.5
			has_ethic = ethic_fanatic_xenophobe
		}
		modifier = {
			factor = 0.75
			has_ethic = ethic_xenophobe
		}
		modifier = {
			factor = 1.25
			has_ethic = ethic_xenophile
		}
		modifier = {
			factor = 1.5
			has_ethic = ethic_fanatic_xenophile
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
		modifier = {
			factor = 0
			NOT = { has_country_flag = first_alien_life }
		}
		modifier = {
			factor = 0
			has_authority = auth_hive_mind
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 0.5
			has_ethic = ethic_fanatic_xenophobe
		}
		modifier = {
			factor = 0.75
			has_ethic = ethic_xenophobe
		}
		modifier = {
			factor = 1.25
			has_ethic = ethic_xenophile
		}
		modifier = {
			factor = 1.5
			has_ethic = ethic_fanatic_xenophile
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
		modifier = {
			factor = 0
			NOT = { has_country_flag = first_alien_life }
		}
	}
}

#Gene Banks
tech_gene_banks = {
	cost = @tier2cost2
	area = society
	tier = 2
	category = { biology }
	is_rare = yes
	prerequisites = { "tech_cloning" }
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0.20
			NOR = {
				research_leader = {
					area = society
					has_trait = "leader_trait_expertise_biology"
				}
				research_leader = {
					area = society
					has_trait = "leader_trait_curator"
				}
			}
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_spark_of_genius"
			}
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = society
				has_level > 4
			}
		}
		modifier = {
			factor = 0.75
			research_leader = {
				area = society
				has_level < 2
			}
		}
	}
	
	ai_weight = {
		factor = 3 #rare tech
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
}

tech_gene_seed_purification = {
	cost = @tier3cost2
	area = society
	category = { biology }
	tier = 3
	is_rare = yes
	prerequisites = { "tech_gene_tailoring" }
	is_reverse_engineerable = no
	weight = 0
	
	# unlocks gene-warrior armies
	
	weight_modifier = {
		modifier = {
			factor = 0
			host_has_dlc = "Utopia"
		}
		modifier = {
			factor = 1.25
			has_ethic = ethic_militarist
		}
		modifier = {
			factor = 1.5
			has_ethic = ethic_fanatic_militarist
		}
		modifier = {
			factor = 0.10
			NOR = {
				research_leader = {
					area = society
					has_trait = "leader_trait_expertise_biology"
				}
				research_leader = {
					area = society
					has_trait = "leader_trait_curator"
				}
			}
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_spark_of_genius"
			}
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = society
				has_level > 4
			}
		}
		modifier = {
			factor = 0.75
			NOT = {
				research_leader = {
					area = society
					has_level > 3
				}
			}
		}
	}
	
	ai_weight = {
		factor = 3 #rare tech
		modifier = {
			factor = 1.25
			has_ethic = ethic_militarist
		}
		modifier = {
			factor = 1.5
			has_ethic = ethic_fanatic_militarist
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
}

tech_morphogenetic_field_mastery = {
	cost = @tier2cost3
	area = society
	category = { biology }
	tier = 2
	is_rare = yes
	prerequisites = { "tech_epigenetic_triggers" }
	is_reverse_engineerable = no
	weight = 0
	
	# unlocks xeno-armies
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 1.25
			has_ethic = ethic_militarist
		}
		modifier = {
			factor = 1.5
			has_ethic = ethic_fanatic_militarist
		}
		modifier = {
			factor = 0.20
			NOR = {
				research_leader = {
					area = society
					has_trait = "leader_trait_expertise_biology"
				}
				research_leader = {
					area = society
					has_trait = "leader_trait_curator"
				}
			}
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_maniacal"
			}
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_spark_of_genius"
			}
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = society
				has_level > 4
			}
		}
		modifier = {
			factor = 0.75
			NOT = {
				research_leader = {
					area = society
					has_level > 2
				}
			}
		}
	}
	
	ai_weight = {
		factor = 3 #rare tech
		modifier = {
			factor = 1.25
			has_ethic = ethic_militarist
		}
		modifier = {
			factor = 1.5
			has_ethic = ethic_fanatic_militarist
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
}

#Gene Tailoring
tech_gene_tailoring = {
	cost = @tier2cost4
	area = society
	tier = 2
	category = { biology }	
	prerequisites = { "tech_cloning" }
	is_reverse_engineerable = no
	weight = 0
	
	gateway = biological
	
	modifier = {
		trait_points = 1
	}
	
	feature_flags = {
		modify_traits
		pop_self_modification
	}
	
	weight_modifier = {
		factor = 1.5 	# genetech needs to be a bit more common
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
}

tech_genetic_resequencing = {
	cost = @tier3cost3
	area = society
	tier = 3
	is_rare = yes
	category = { biology }	
	is_reverse_engineerable = no
	weight = 0
	
	gateway = biological
	
	feature_flags = {
		advanced_gene_modding
		add_advanced_traits
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
}

#Targeted Gene Expressions
tech_gene_expressions = {
	cost = @tier3cost2
	area = society
	category = { biology }
	tier = 3
	is_rare = yes
	prerequisites = { "tech_gene_tailoring" }
	is_reverse_engineerable = no
	weight = 0
	
	modifier = {
		trait_points = 1
	}
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0.50
			NOR = {
				research_leader = {
					area = society
					has_trait = "leader_trait_expertise_biology"
				}
				research_leader = {
					area = society
					has_trait = "leader_trait_curator"
				}
			}
		}
		modifier = {
			factor = 1.50
			research_leader = {
				area = society
				has_trait = "leader_trait_spark_of_genius"
			}
		}
		modifier = {
			factor = 1.50
			OR = {
				has_ethic = ethic_authoritarian
				has_ethic = ethic_fanatic_authoritarian
			}
		}
		modifier = {
			factor = 0.10
			OR = {
				has_ethic = ethic_egalitarian
				has_ethic = ethic_fanatic_egalitarian
			}
		}
	}	
	
	ai_weight = {
		factor = 3 #rare tech
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_biology"
			}
		}
	}
}

#Regenerative Hull Tissue
tech_regenerative_hull_tissue = {
	area = society
	cost = @tier3cost3
	tier = 3
	is_rare = yes
	category = { materials }
	ai_update_type = military
	is_reverse_engineerable = no
	weight = 0
	
	weight_modifier = { 
		factor = 0 		# Can only be reverse engineered
	}
}

# ##################
# Fleet Organization
# ##################

#Doctrine: Fleet Support 
tech_doctrine_fleet_support = {
	cost = @tier1cost2
	area = society
	tier = 1
	category = { military_theory }
	prerequisites = { "tech_doctrine_fleet_size_1" }
	is_reverse_engineerable = no
	weight = 0
	
	#unlocks crew quarters and engineering bay modules
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			has_ethic = ethic_militarist
		}
		modifier = {
			factor = 1.5
			has_ethic = ethic_fanatic_militarist
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_military_theory"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			has_ethic = ethic_militarist
		}
		modifier = {
			factor = 1.5
			has_ethic = ethic_fanatic_militarist
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_military_theory"
			}
		}
	}
}


# ##################
# Army Organization
# ##################


# ##################
# Psionics
# ##################

#Psionic Theory
tech_psionic_theory = {
	cost = @tier2cost4
	area = society
	tier = 2
	category = { psionics }
	is_rare = yes
	weight = 0
	is_reverse_engineerable = no
	
	gateway = psionics
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0
			NOR = {
				has_ethic = "ethic_spiritualist"
				has_ethic = "ethic_fanatic_spiritualist"
			}
		}
		modifier = {
			factor = 1.10
			research_leader = {
				area = society
				has_level > 2
				has_trait = "leader_trait_expertise_psionics"
			}
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = society
				has_level > 3
				has_trait = "leader_trait_expertise_psionics"
			}
		}
		modifier = {
			factor = 2
			research_leader = {
				area = society
				has_level > 4
				has_trait = "leader_trait_expertise_psionics"
			}
		}
		modifier = {
			factor = 2
			has_ethic = "ethic_fanatic_spiritualist"
		}
		modifier = {
			factor = 5
			research_leader = {
				area = society
				has_trait = "leader_trait_maniacal"
			}
		}
		modifier = {
			factor = 0
			AND = {
				OR = {
					has_ethic = "ethic_materialist"
					has_ethic = "ethic_fanatic_materialist"
				}
				NOT = {
					research_leader = {
						area = society
						has_trait = "leader_trait_expertise_psionics"
					}
				}
			}
		}
	}
	
	ai_weight = {
		factor = 5 #very rare tech
		modifier = {
			factor = 1.5
			OR = {
				has_ethic = ethic_spiritualist
				has_ethic = ethic_fanatic_spiritualist
			}
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_psionics"
			}
		}
	}
}

#Telepathy
tech_telepathy = {
	cost = @tier3cost1
	area = society
	tier = 3
	category = { psionics }
	is_rare = yes	
	prerequisites = { "tech_psionic_theory" }
	weight = 0
	is_reverse_engineerable = no
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0
			host_has_dlc = "Utopia"
		}
		modifier = {
			factor = 0.50
			NOT = {
				research_leader = {
					area = society
					has_trait = "leader_trait_expertise_psionics"
				}
			}
		}
		modifier = {
			factor = 1.10
			research_leader = {
				area = society
				has_level > 2
				has_trait = "leader_trait_expertise_psionics"
			}
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = society
				has_level > 3
				has_trait = "leader_trait_expertise_psionics"
			}
		}
		modifier = {
			factor = 2
			research_leader = {
				area = society
				has_level > 4
				has_trait = "leader_trait_expertise_psionics"
			}
		}
		modifier = {
			factor = 2
			has_ethic = "ethic_spiritualist"
		}
		modifier = {
			factor = 3
			has_ethic = "ethic_fanatic_spiritualist"
		}
		modifier = {
			factor = 4
			research_leader = {
				area = society
				has_trait = "leader_trait_maniacal"
			}
		}
	}
	
	ai_weight = {
		factor = 5 #very rare tech
		modifier = {
			factor = 1.5
			OR = {
				has_ethic = ethic_spiritualist
				has_ethic = ethic_fanatic_spiritualist
			}
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_psionics"
			}
		}
	}
}

tech_precognition_interface = {
	cost = @tier3cost1
	area = society
	tier = 3
	category = { psionics }
	is_rare = yes
	prerequisites = { "tech_telepathy" }
	weight = 0
	is_reverse_engineerable = no
	
	# unlocks precognitive interface-component, combat-computer granting evasion to ships
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_COMBAT_COMPUTER_PRECOG_TITLE"
			desc = "TECH_UNLOCK_COMBAT_COMPUTER_PRECOG_DESC"
		}
	}
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0
			host_has_dlc = "Utopia"
		}
		modifier = {
			factor = 0.50
			NOT = {
				research_leader = {
					area = society
					has_trait = "leader_trait_expertise_psionics"
				}
			}
		}
		modifier = {
			factor = 1.10
			research_leader = {
				area = society
				has_level > 2
				has_trait = "leader_trait_expertise_psionics"
			}
		}
		modifier = {
			factor = 1.5
			research_leader = {
				area = society
				has_level > 3
				has_trait = "leader_trait_expertise_psionics"
			}
		}
		modifier = {
			factor = 2
			research_leader = {
				area = society
				has_level > 4
				has_trait = "leader_trait_expertise_psionics"
			}
		}
		modifier = {
			factor = 2
			has_ethic = "ethic_spiritualist"
		}
		modifier = {
			factor = 3
			has_ethic = "ethic_fanatic_spiritualist"
		}
		modifier = {
			factor = 4
			research_leader = {
				area = society
				has_trait = "leader_trait_maniacal"
			}
		}
	}
	
	ai_weight = {
		factor = 5 #very rare tech
		modifier = {
			factor = 1.25
			has_ethic = ethic_militarist
		}
		modifier = {
			factor = 1.5
			has_ethic = ethic_fanatic_militarist
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_psionics"
			}
		}
	}
}

tech_psi_jump_drive_1 = {
	cost = @tier3cost4
	area = society
	tier = 3
	category = { psionics }
	ai_update_type = all
	is_rare = yes
	is_dangerous = yes
	prerequisites = { "tech_precognition_interface" }
	weight = 0
	is_dangerous = yes
	is_reverse_engineerable = no
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_PSI_JUMPDRIVE_1_TITLE"
			desc = "TECH_UNLOCK_PSI_JUMPDRIVE_1_DESC"
		}
	}
	
	weight_modifier = {
		factor = 0.1
		modifier = {
			factor = 0
			host_has_dlc = "Utopia"
		}
		modifier = {
			factor = 0.1
			NOR = {
				research_leader = {
					area = society
					has_trait = "leader_trait_expertise_psionics"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_curator"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_spark_of_genius"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_maniacal"
				}
			}
		}		
	}
}

#Xeno Integration
#tech_xeno_integration = {
#	cost = @tier2cost1
#	area = society
#	category = { biology }
#	tier = 2	
#	weight = @tier2weight1
#	
#	# leaders from other species available
#	
#	feature_flags = { xeno_integration }
#	
#	weight_modifier = {	
#		factor = 2
#		modifier = {
#			factor = 1.25
#			research_leader = {
#				area = society
#				has_trait = "leader_trait_expertise_statecraft"
#			}
#		}
#		modifier = {
#			factor = 1.25
#			research_leader = {
#				area = society
#				has_trait = "leader_trait_expertise_statecraft"
#			}
#		}
#		modifier = {
#			factor = 0
#			OR = {
#				has_ethic = ethic_xenophobe
#				has_ethic = ethic_fanatic_xenophobe
#				has_ethic = ethic_xenophile
#				has_ethic = ethic_fanatic_xenophile
#			}
#		}
#		modifier = { # checks if there are no foreign Pops (ie all are dominant species or robots)
#			factor = 0
#			NOT = {
#				any_owned_pop = {
#					NOR = {
#						is_same_species = root
#						is_species = "ROBOT_POP_SPECIES_1"
#						is_species = "ROBOT_POP_SPECIES_2"
#						is_species = "ROBOT_POP_SPECIES_3"
#					}
#				}
#			}
#		}
#	}
#}

# ##################
# Empire Management
# ##################


#Subdermal Implants
tech_subdermal_stimulation = {
	cost = @tier2cost1
	area = society
	tier = 2
	category = { statecraft }
	weight = 0
	is_reverse_engineerable = no
	is_rare = yes
	
	# unlocks Chemical Bliss living standard

	feature_flags = {
		bliss_standard
	}
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0
			has_ethic = ethic_hive_mind
		}
		modifier = {
			factor = 1.25
			NOR = {
				has_ethic = ethic_egalitarian
				has_ethic = ethic_fanatic_egalitarian
			}
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_statecraft"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 3
			NOR = {
				has_ethic = ethic_egalitarian
				has_ethic = ethic_fanatic_egalitarian
			}
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_statecraft"
			}
		}
	}
}

# ##################
# Social Engineering
# ##################

#Artificial Moral Codes
tech_artificial_moral_codes = {
	cost = @tier2cost2
	area = society
	tier = 2
	category = { statecraft }
	is_reverse_engineerable = no
	weight = 0
	
	modifier = {
		pop_resettlement_cost_mult = -0.1
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_statecraft"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_statecraft"
			}
		}
	}
}

#Synthetic Thought Patterns
tech_synthetic_thought_patterns = {
	cost = @tier2cost4
	area = society
	tier = 2
	category = { statecraft }
	prerequisites = { "tech_artificial_moral_codes" }
	is_reverse_engineerable = no
	weight = 0
	
	modifier = {
		edict_influence_cost = -0.15
		pop_resettlement_cost_mult = -0.1
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_statecraft"
			}
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_spark_of_genius"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_statecraft"
			}
		}
	}
}

#Will to Power
tech_will_to_power = {
	cost = @tier3cost3
	area = society
	tier = 3
	category = { statecraft }
	is_rare = yes	
	prerequisites = { "tech_synthetic_thought_patterns" }
	is_reverse_engineerable = no
	weight = 0

	# unlocks spaceport-module: orbital mind control laser
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0.20
			NOR = {
				research_leader = {
					area = society
					has_trait = "leader_trait_maniacal"
				}
				research_leader = {
					area = society
					has_trait = "leader_trait_curator"
				}
			}
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_statecraft"
			}
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_spark_of_genius"
			}
		}
		modifier = {
			factor = 5
			research_leader = {
				area = society
				has_trait = "leader_trait_maniacal"
			}
		}
	}
	
	ai_weight = {
		factor = 3 #rare tech
		modifier = {
			factor = 1.25
			research_leader = {
				area = society
				has_trait = "leader_trait_expertise_statecraft"
			}
		}
	}
}

# ##################
# Unity
# ##################


# ##################
# Other
# ##################

