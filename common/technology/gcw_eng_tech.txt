tech_space_construction = {
	cost = 0
	area = engineering
	tier = 0
	category = { industry }
	start_tech = yes
	
	weight = 50
	
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_CONSTRUCTION_SHIP_CONSTRUCTION_TITLE"
			desc = "TECH_UNLOCK_CONSTRUCTION_SHIP_CONSTRUCTION_DESC"
		}
		# used to separate the entries
		custom = {
			title = "TECH_UNLOCK_MINING_STATION_TITLE"
			desc = "TECH_UNLOCK_MINING_STATION_DESC"
		}
		# used to separate the entries
		custom = {
			title = "TECH_UNLOCK_RESEARCH_STATION_TITLE"
			desc = "TECH_UNLOCK_RESEARCH_STATION_DESC"
		}
	}
	
	ai_weight = {
		factor = 10 #very important tech
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_spaceport_1 = {
	cost = 0
	area = engineering
	start_tech = yes
	category = { voidcraft }
	tier = 0
	
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SCIENCE_SHIP_CONSTRUCTION_TITLE"
			desc = "TECH_UNLOCK_SCIENCE_SHIP_CONSTRUCTION_DESC"
		}
		custom = {
			title = "TECH_UNLOCK_SPACEPORT_CONSTRUCTION_TITLE"
			desc = "TECH_UNLOCK_SPACEPORT_CONSTRUCTION_DESC"
		}
	}
}

tech_spaceport_2 = {
	cost = 300
	area = engineering
	tier = 1
	category = { voidcraft }
	prerequisites = { "tech_space_construction" }
	weight = 200
	
	gateway = ship
	
	modifier = {
		max_minerals = 2000
	}
	
	feature_flags = { spaceport_level_2 }
	
	weight_modifier = {
		modifier = {
			factor = 10
			any_neighbor_country = {
				has_technology = tech_spaceport_2
			}
		}	
		modifier = {
			factor = 2
			years_passed > 4
		}
		modifier = {
			factor = 3
			years_passed > 7
		}
		modifier = {
			factor = 4
			years_passed > 10
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		factor = 100
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_spaceport_3 = {
	cost = 600
	area = engineering
	tier = 2
	category = { voidcraft }	
	prerequisites = { "tech_spaceport_2" }
	weight = 200
	
	gateway = ship
	
	modifier = {
		max_minerals = 2000
	}
	
	feature_flags = { spaceport_level_3 }
	
	weight_modifier = {
		modifier = {
			factor = 0.1
			NOT = { years_passed > 10 }
		}	
		modifier = {
			factor = 10
			any_neighbor_country = {
				has_technology = tech_spaceport_3
			}
		}		
		modifier = {
			factor = 2
			years_passed > 15
		}
		modifier = {
			factor = 3
			years_passed > 20
		}
		modifier = {
			factor = 4
			years_passed > 30
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		factor = 100
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_spaceport_4 = {
	cost = 1400
	area = engineering
	tier = 3
	category = { voidcraft }	
	prerequisites = { "tech_spaceport_3" }
	weight = 200
	
	gateway = ship
		
	modifier = {
		max_minerals = 2000
	}		
		
	feature_flags = { spaceport_level_4 }
	
	weight_modifier = {
		modifier = {
			factor = 0.1
			NOT = { years_passed > 15 }
		}
		modifier = {
			factor = 10
			any_neighbor_country = {
				has_technology = tech_spaceport_4
			}
		}		
		modifier = {
			factor = 2
			years_passed > 20
		}
		modifier = {
			factor = 3
			years_passed > 25
		}
		modifier = {
			factor = 4
			years_passed > 30
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		factor = 100
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_spaceport_5 = {
	cost = 3000
	area = engineering
	tier = 4
	category = { voidcraft }	
	prerequisites = { "tech_spaceport_4" }
	weight = 200
	
	gateway = ship
	
	modifier = {
		max_minerals = 2000
	}	
	
	feature_flags = { spaceport_level_5 }
	
	weight_modifier = {
		modifier = {
			factor = 0.1
			NOT = { years_passed > 20 }
		}	
		modifier = {
			factor = 10
			any_neighbor_country = {
				has_technology = tech_spaceport_5
			}
		}		
		modifier = {
			factor = 2
			years_passed > 30
		}
		modifier = {
			factor = 3
			years_passed > 35
		}
		modifier = {
			factor = 4
			years_passed > 40
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		factor = 100
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_spaceport_6 = {
	cost = 4600
	area = engineering
	tier = 5
	category = { voidcraft }	
	prerequisites = { "tech_spaceport_5" }
	weight = 200
	
	modifier = {
		max_minerals = 2000
	}
	
	feature_flags = { spaceport_level_6 }
	
	weight_modifier = {
		modifier = {
			factor = 0.1
			NOT = { years_passed > 50 }
		}	
		modifier = {
			factor = 10
			any_neighbor_country = {
				has_technology = tech_spaceport_6
			}
		}
		modifier = {
			factor = 2
			years_passed > 60
		}
		modifier = {
			factor = 3
			years_passed > 65
		}
		modifier = {
			factor = 4
			years_passed > 70
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		factor = 100
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_space_defense_station_improvement = {
	cost = 5400
	area = engineering
	tier = 5
	category = { voidcraft }		
	prerequisites = { "tech_space_defense_station_2" }
	is_rare = yes
	weight = 100
	
	modifier = {
		shipclass_military_station_build_cost_mult = -0.25
		shipclass_military_station_hit_points_mult = 0.10
	}
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0.5
			NOR = {
				research_leader = {
					area = engineering
					has_trait = "leader_trait_expertise_voidcraft"
				}
				research_leader = {
					area = engineering
					has_trait = "leader_trait_curator"
				}
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				has_ethic = ethic_militarist
				has_ethic = ethic_fanatic_militarist
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_defense_station = {
	cost = 0
	area = engineering
	category = { industry }
	tier = 0
	start_tech = yes
}

tech_golan_1 = {
	cost = 600
	area = engineering
	tier = 2
	category = { voidcraft }	
	prerequisites = { "tech_defense_station" }
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 2
			is_ai = yes
		}		
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				has_ethic = ethic_militarist
				has_ethic = ethic_fanatic_militarist
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_golan_2 = {
	cost = 1400
	area = engineering
	tier = 3
	category = { voidcraft }	
	prerequisites = { "tech_golan_1" }
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 2
			is_ai = yes
		}		
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				has_ethic = ethic_militarist
				has_ethic = ethic_fanatic_militarist
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_golan_3 = {
	cost = 3000
	area = engineering
	tier = 4
	category = { voidcraft }	
	prerequisites = { "tech_golan_2" }
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 2
			is_ai = yes
		}		
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				has_ethic = ethic_militarist
				has_ethic = ethic_fanatic_militarist
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_fleet_operations = {
	cost = 4600
	area = engineering
	tier = 5
	category = { voidcraft }	
	prerequisites = { "tech_golan_3" }
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 2
			is_ai = yes
		}		
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				has_ethic = ethic_militarist
				has_ethic = ethic_fanatic_militarist
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_gcwcorvette = {
	cost = 0
	area = engineering
	category = { industry }
	tier = 0
	start_tech = yes
	weight = 0

	gateway = ships
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SHIP_GCWCORVETTE_TITLE"
			desc = "TECH_UNLOCK_SHIP_GCWCORVETTE_DESC"
		}
	}
}

tech_gcwfrigate = {
	cost = 200
	area = engineering
	tier = 1
	category = { industry }	
	prerequisites = { "tech_gcwcorvette" "tech_spaceport_2" }
	weight = 200
	
	gateway = ships
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SHIP_GCWFRIGATE_TITLE"
			desc = "TECH_UNLOCK_SHIP_GCWFRIGATE_DESC"
		}
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_gcwcruiser = {
	cost = 300
	area = engineering
	tier = 1
	category = { industry }	
	prerequisites = { "tech_gcwfrigate" }
	weight = 200
	
	gateway = ships
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SHIP_GCWCRUISER_TITLE"
			desc = "TECH_UNLOCK_SHIP_GCWCRUISER_DESC"
		}
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_heavy_cruiser = {
	cost = 600
	area = engineering
	tier = 2
	category = { industry }	
	prerequisites = { "tech_gcwcruiser" }
	weight = 200
	
	gateway = ships
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SHIP_HEAVYCRUISER_TITLE"
			desc = "TECH_UNLOCK_SHIP_HEAVYCRUISER_DESC"
		}
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_adv_frigate = {
	cost = 800
	area = engineering
	tier = 2
	category = { industry }	
	prerequisites = { "tech_gcwfrigate" }
	weight = 200
	
	gateway = ships
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SHIP_ADVFRIGATE_TITLE"
			desc = "TECH_UNLOCK_SHIP_ADVFRIGATE_DESC"
		}
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_star_destroyer = {
	cost = 1400
	area = engineering
	tier = 3
	category = { industry }	
	prerequisites = { "tech_heavy_cruiser" }
	weight = 200
	
	gateway = ships
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SHIP_STARDESTROYER_TITLE"
			desc = "TECH_UNLOCK_SHIP_STARDESTROYER_DESC"
		}
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_interdictor = {
	cost = 1800
	area = engineering
	tier = 3
	category = { industry }	
	prerequisites = { "tech_adv_frigate" }
	weight = 200
	
	gateway = ships
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SHIP_INTERDICTOR_TITLE"
			desc = "TECH_UNLOCK_SHIP_INTERDICTOR_DESC"
		}
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_adv_star_destroyer = {
	cost = 3000
	area = engineering
	tier = 4
	category = { industry }	
	prerequisites = { "tech_star_destroyer" }
	weight = 200
	
	gateway = ships
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SHIP_ADVSTARDESTROYER_TITLE"
			desc = "TECH_UNLOCK_SHIP_ADVSTARDESTROYER_DESC"
		}
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_heavy_interdictor = {
	cost = 3800
	area = engineering
	tier = 4
	category = { industry }	
	prerequisites = { "tech_interdictor" }
	weight = 200
	
	gateway = ships
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SHIP_HEAVYINTERDICTOR_TITLE"
			desc = "TECH_UNLOCK_SHIP_HEAVYINTERDICTOR_DESC"
		}
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_battlecruiser = {
	cost = 4600
	area = engineering
	tier = 5
	category = { industry }	
	prerequisites = { "tech_adv_star_destroyer" }
	weight = 200
	
	gateway = ships
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SHIP_BATTLECRUISER_TITLE"
			desc = "TECH_UNLOCK_SHIP_BATTLECRUISER_DESC"
		}
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_dreadnought = {
	cost = 5400
	area = engineering
	tier = 5
	category = { industry }	
	prerequisites = { "tech_battlecruiser" }
	weight = 200
	
	gateway = ships
	prereqfor_desc = {
		ship = {
			title = "TECH_UNLOCK_SHIP_DREADNOUGHT_TITLE"
			desc = "TECH_UNLOCK_SHIP_DREADNOUGHT_DESC"
		}
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_sublight_engine = {
	area = engineering
	cost = 0
	start_tech = yes
	tier = 0
	category = { rocketry }
	ai_update_type = all
}

tech_ion_engine = {
	area = engineering
	cost = 200
	tier = 1
	category = { rocketry }
	ai_update_type = all	
	prerequisites = { "tech_sublight_engine" }
	weight = 100
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
		modifier = {
			factor = 2
			has_technology = tech_spaceport_2
		}
		modifier = {
			factor = 2
			has_technology = tech_spaceport_3
		}
	}
	
	ai_weight = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
	}
}

tech_improved_ion_engine = {
	area = engineering
	cost = 600
	tier = 2
	category = { rocketry }
	ai_update_type = all	
	prerequisites = { "tech_ion_engine" }
	weight = 100
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
		modifier = {
			factor = 2
			has_technology = tech_spaceport_4
		}
		modifier = {
			factor = 2
			has_technology = tech_spaceport_5
		}
	}
	
	ai_weight = {
		factor = 1.25 #important component
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
	}
}

tech_fusial_thrust_engine = {
	area = engineering
	cost = 3000
	tier = 4
	category = { rocketry }
	ai_update_type = all	
	prerequisites = { "tech_improved_ion_engine" }
	weight = 100
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
		modifier = {
			factor = 2
			has_technology = tech_spaceport_6
		}
	}
	
	ai_weight = {
		factor = 1.25 #important component
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_rocketry"
			}
		}
	}
}

tech_durasteel_armor = {
	area = engineering
	cost = 0
	start_tech = yes
	tier = 0
	category = { materials }
	ai_update_type = all
}

tech_titanium_armor = {
	area = engineering
	cost = 200
	tier = 1
	category = { materials }
	ai_update_type = all	
	prerequisites = { "tech_durasteel_armor" }
	weight = 100
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
}

tech_duranium_armor = {
	area = engineering
	cost = 600
	tier = 2
	category = { materials }
	ai_update_type = all	
	prerequisites = { "tech_titanium_armor" }
	weight = 100
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
}

tech_impervium_armor = {
	area = engineering
	cost = 3000
	tier = 2
	category = { materials }
	ai_update_type = all	
	prerequisites = { "tech_duranium_armor" }
	weight = 100
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_materials"
			}
		}
	}
}

tech_basic_industry = {
	cost = 0
	area = engineering
	start_tech = yes
	category = { industry }
	tier = 0
	icon = "t_space_construction"
}

tech_mining_network_2 = {
	cost = 200
	area = engineering
	tier = 1
	category = { industry }	
	prerequisites = { "tech_basic_industry" }
	weight = 50
	
	modifier = {
		max_minerals = 1000
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_mining_network_3 = {
	cost = 600
	area = engineering
	tier = 2
	category = { industry }
	prerequisites = { "tech_mining_network_2" "tech_mineral_processing_1" "tech_colonial_centralization" }
	weight = 50
	
	modifier = {
		max_minerals = 1000
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_mining_network_4 = {
	cost = 3000
	area = engineering
	tier = 4
	category = { industry }
	prerequisites = { "tech_mining_network_3" "tech_galactic_administration" }
	weight = 50
	
	modifier = {
		max_minerals = 1000
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_mining_network_5 = {
	cost = 4600
	area = engineering
	tier = 5
	category = { industry }
	prerequisites = { "tech_mining_network_3" "tech_galactic_administration" }
	weight = 50
	
	modifier = {
		max_minerals = 1000
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_mineral_processing_1 = {
	cost = 800
	area = engineering
	tier = 2
	category = { industry }	
	prerequisites = { "tech_mining_network_2" }
	weight = 50
	
	modifier = {
		max_minerals = 1000
	}	
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_mineral_processing_2 = {
	cost = 3800
	area = engineering
	tier = 4
	category = { industry }	
	prerequisites = { "tech_mineral_processing_1" "tech_colonial_centralization" "tech_mining_network_3" }
	weight = 50
	
	modifier = {
		max_minerals = 1000
	}	
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_assembly_pattern  = {
	cost = 1800
	area = engineering
	tier = 3
	category = { industry }
	prerequisites = { "tech_mineral_processing_1" }
	weight = 50
	
	modifier = {
		planet_building_build_speed_mult = 0.25
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			OR = {
				has_ethic = ethic_militarist
				has_ethic = ethic_fanatic_militarist
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_construction_templates  = {
	cost = 3800
	area = engineering
	tier = 4
	is_rare = yes
	category = { industry }		
	prerequisites = { "tech_assembly_pattern" }
	weight = 50
	
	modifier = {
		planet_building_build_speed_mult = 0.50
	}
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0.5
			NOR = {
				research_leader = {
					area = engineering
					has_trait = "leader_trait_expertise_industry"
				}
				research_leader = {
					area = engineering
					has_trait = "leader_trait_curator"
				}
			}
		}
		modifier = {
			factor = 1.25
			OR = {
				has_ethic = ethic_militarist
				has_ethic = ethic_fanatic_militarist
			}
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_basic_droids = {
	cost = 0
	area = engineering
	start_tech = yes
	category = { industry }
	tier = 0
	icon = "t_space_construction"
}

tech_assault_armies = {
	cost = 0
	area = engineering
	tier = 0
	category = { voidcraft }
	prerequisites = { "tech_defense_army" }
	start_tech = yes
}

tech_powered_exoskeletons = {
	cost = 1400
	area = engineering
	tier = 3
	category = { industry }
	prerequisites = { "tech_basic_industry" }
	weight = 45
	
	gateway = robotics
	
	modifier = {
		army_damage_mult = 0.05
		tile_resource_minerals_mult = 0.05
	}
	
	weight_modifier = {
		factor = 1.5		
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}			
		}
	}
	
	ai_weight = {
		factor = 2
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_tie_1 = {
	cost = 0
	area = engineering
	tier = 0
	ai_update_type = military
	category = { voidcraft }
	is_reverse_engineerable = no
	weight = 0
}

tech_tie_2 = {
	cost = 300
	area = engineering
	tier = 1
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_tie_1" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_tie_3 = {
	cost = 1800
	area = engineering
	tier = 3
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_tie_2" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_tie_4 = {
	cost = 5400
	area = engineering
	tier = 5
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_tie_3" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_hapan_snub_1 = {
	cost = 0
	area = engineering
	tier = 0
	ai_update_type = military
	category = { voidcraft }
	is_reverse_engineerable = no
	weight = 0
}

tech_hapan_snub_2 = {
	cost = 300
	area = engineering
	tier = 1
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_hapan_snub_1" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_hapan_snub_3 = {
	cost = 800
	area = engineering
	tier = 2
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_hapan_snub_2" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_hapan_snub_4 = {
	cost = 1800
	area = engineering
	tier = 3
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_hapan_snub_3" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_rebel_snub_1 = {
	cost = 0
	area = engineering
	tier = 0
	ai_update_type = military
	category = { voidcraft }
	is_reverse_engineerable = no
	weight = 0
}

tech_rebel_snub_2 = {
	cost = 300
	area = engineering
	tier = 1
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_rebel_snub_1" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_rebel_snub_3 = {
	cost = 800
	area = engineering
	tier = 2
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_rebel_snub_2" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_rebel_snub_4 = {
	cost = 1800
	area = engineering
	tier = 3
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_rebel_snub_3" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_hutt_snub_1 = {
	cost = 0
	area = engineering
	tier = 0
	ai_update_type = military
	category = { voidcraft }
	is_reverse_engineerable = no
	weight = 0
}

tech_hutt_snub_2 = {
	cost = 300
	area = engineering
	tier = 1
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_hutt_snub_1" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_hutt_snub_3 = {
	cost = 800
	area = engineering
	tier = 2
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_hutt_snub_2" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_hutt_snub_4 = {
	cost = 1800
	area = engineering
	tier = 3
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_hutt_snub_3" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_mando_snub_1 = {
	cost = 0
	area = engineering
	tier = 0
	ai_update_type = military
	category = { voidcraft }
	is_reverse_engineerable = no
	weight = 0
}

tech_mando_snub_2 = {
	cost = 300
	area = engineering
	tier = 1
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_mando_snub_1" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_mando_snub_3 = {
	cost = 800
	area = engineering
	tier = 2
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_mando_snub_2" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_mando_snub_4 = {
	cost = 1800
	area = engineering
	tier = 3
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_mando_snub_3" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_cis_snub_1 = {
	cost = 0
	area = engineering
	tier = 0
	ai_update_type = military
	category = { voidcraft }
	is_reverse_engineerable = no
	weight = 0
}

tech_cis_snub_2 = {
	cost = 300
	area = engineering
	tier = 1
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_cis_snub_1" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_cis_snub_3 = {
	cost = 800
	area = engineering
	tier = 2
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_cis_snub_2" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_cis_snub_4 = {
	cost = 1800
	area = engineering
	tier = 3
	ai_update_type = military
	category = { voidcraft }	
	prerequisites = { "tech_cis_snub_3" }
	is_reverse_engineerable = no
	weight = 100
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_mega_engineering = {
	area = engineering
	cost = 8000
	tier = 5
	category = { voidcraft }
	ai_update_type = all
	prerequisites = { "tech_dreadnought" "tech_spaceport_6" }
	weight = 50
	is_rare = yes
	
	modifier = {
		max_minerals = 20000
		planet_building_build_speed_mult = 0.10
	}
	
	weight_modifier = {
		factor = 0.25
		modifier = {
			factor = 0.5
			NOR = {
				research_leader = {
					area = engineering
					has_trait = "leader_trait_expertise_voidcraft"
				}
				research_leader = {
					area = engineering
					has_trait = "leader_trait_curator"
				}
				research_leader = {
					area = engineering
					has_trait = "leader_trait_maniacal"
				}				
			}
		}
		modifier = {
			factor = 2
			has_ascension_perk = ap_voidborn
		}		
		modifier = {
			factor = 2
			has_ascension_perk = ap_master_builders
		}

		### Enabled for Vanilla so people can repair damaged Ring Worlds
		#modifier = {
		#	factor = 0
		#	NOT = { 
		#		host_has_dlc = "Utopia" 
		#	}
		#}
	}
	
	ai_weight = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}

tech_superlaser_construction = {
	area = engineering
	cost = 8000
	tier = 5
	category = { voidcraft }
	ai_update_type = all
	prerequisites = { "tech_mega_engineering" }
	weight = 50
	
	weight_modifier = {
		factor = 0.25
		modifier = {
			factor = 0.5
			NOR = {
				research_leader = {
					area = engineering
					has_trait = "leader_trait_expertise_voidcraft"
				}
				research_leader = {
					area = engineering
					has_trait = "leader_trait_curator"
				}
				research_leader = {
					area = engineering
					has_trait = "leader_trait_maniacal"
				}				
			}
		}
		modifier = {
			factor = 2
			has_ascension_perk = ap_voidborn
		}		
		modifier = {
			factor = 2
			has_ascension_perk = ap_master_builders
		}
	}
	
	ai_weight = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = engineering
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
}