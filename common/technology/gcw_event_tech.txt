tech_heavy_cruiser_mid2 = {
	cost = 600
	area = engineering
	tier = 2
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_heavy_cruiser_mid3 = {
	cost = 600
	area = engineering
	tier = 2
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_adv_frigate_mid2 = {
	cost = 800
	area = engineering
	tier = 2
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_adv_frigate_mid3 = {
	cost = 800
	area = engineering
	tier = 2
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_star_destroyer_mid1 = {
	cost = 1400
	area = engineering
	tier = 3
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_star_destroyer_mid2 = {
	cost = 1400
	area = engineering
	tier = 3
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_star_destroyer_mid3 = {
	cost = 1400
	area = engineering
	tier = 3
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_heavy_cruiser_mid4 = {
	cost = 1400
	area = engineering
	tier = 3
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_heavy_cruiser_mid5 = {
	cost = 1400
	area = engineering
	tier = 3
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_heavy_cruiser_mid6 = {
	cost = 1400
	area = engineering
	tier = 3
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_adv_star_destroyer_mid1 = {
	cost = 3000
	area = engineering
	tier = 4
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_adv_star_destroyer_mid2 = {
	cost = 3000
	area = engineering
	tier = 4
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_adv_star_destroyer_mid4 = {
	cost = 3000
	area = engineering
	tier = 4
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_heavy_interdictor_mid1 = {
	cost = 3800
	area = engineering
	tier = 4
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_heavy_interdictor_mid2 = {
	cost = 3800
	area = engineering
	tier = 4
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_heavy_interdictor_mid3 = {
	cost = 3800
	area = engineering
	tier = 4
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_battlecruiser_mid1 = {
	cost = 4600
	area = engineering
	tier = 5
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_battlecruiser_mid2 = {
	cost = 4600
	area = engineering
	tier = 5
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_battlecruiser_mid3 = {
	cost = 4600
	area = engineering
	tier = 5
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_battlecruiser_mid4 = {
	cost = 4600
	area = engineering
	tier = 5
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_battlecruiser_mid5 = {
	cost = 4600
	area = engineering
	tier = 5
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_dreadnought_mid1 = {
	cost = 5400
	area = engineering
	tier = 5
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_dreadnought_mid2 = {
	cost = 5400
	area = engineering
	tier = 5
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}

tech_dreadnought_mid3 = {
	cost = 5400
	area = engineering
	tier = 5
	ai_update_type = military
	category = { industry }
	is_reverse_engineerable = no
	weight = 0
}