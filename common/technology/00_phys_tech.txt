##################
### TECH COSTS ###
##################
#If you change any of these, use replace in files so the values are the same across all files

@tier1cost1 = 360
@tier1cost2 = 480
@tier1cost3 = 600
@tier1cost4 = 720

@tier2cost1 = 1000
@tier2cost2 = 1400
@tier2cost3 = 1800
@tier2cost4 = 2200

@tier3cost1 = 3000
@tier3cost2 = 4000
@tier3cost3 = 5000
@tier3cost4 = 6000

####################
### TECH WEIGHTS ###
####################

@tier1weight1 = 100
@tier1weight2 = 95
@tier1weight3 = 90
@tier1weight4 = 85

@tier2weight1 = 75
@tier2weight2 = 70
@tier2weight3 = 65
@tier2weight4 = 60

@tier3weight1 = 50
@tier3weight2 = 45
@tier3weight3 = 40
@tier3weight4 = 35

# ## physics technologies

# ##################
# Science Ship actions
# ##################

# ##################
# Basic Science Lab
# ##################

tech_curator_lab = {
	area = physics
	category = { computing }
	tier = 2
	cost = @tier1cost4
	is_reverse_engineerable = no
	is_rare = yes
	weight = 0
	is_reverse_engineerable = no
	
	#unlocks Curator Exploration Lab
	
	weight_modifier = {
		modifier = {
			factor = 0.50
				has_modifier = "curator_insight"
		}
		modifier = {
			factor = 3
			research_leader = {
				area = physics
				has_trait = "leader_trait_curator"
			}
		}
		modifier = {
			factor = 0
			NOR = {
				research_leader = {
					area = physics
					has_trait = "leader_trait_curator"
				}
				has_modifier = "curator_insight"	
			}
		}
	}
		
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_CURATOR_LAB_1_TITLE"
			desc = "TECH_UNLOCK_CURATOR_LAB_1_DESC"
		}
	}
}

tech_archeology_lab = {
	area = physics
	category = { computing }
	tier = 2
	cost = @tier1cost4
	is_reverse_engineerable = no
	is_rare = yes
	weight = 0
	is_reverse_engineerable = no
	
	#unlocks Curator Archeology Lab
	
	weight_modifier = {
		modifier = {
			factor = 0.50
				has_modifier = "curator_insight"
		}
		modifier = {
			factor = 3
			research_leader = {
				area = physics
				has_trait = "leader_trait_curator"
			}
		}
		modifier = {
			factor = 0
			NOR = {
				research_leader = {
					area = physics
					has_trait = "leader_trait_curator"
				}
				has_modifier = "curator_insight"	
			}
		}
	}
		
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_CURATOR_LAB_2_TITLE"
			desc = "TECH_UNLOCK_CURATOR_LAB_2_DESC"
		}
	}
}


# ##################
# Physics Lab and Computers/AI
# ##################

tech_physics_lab_1 = {
	cost = @tier1cost1
	area = physics
	tier = 1
	category = { computing }
	is_reverse_engineerable = no
	weight = 0
		
	#unlock physics lab lvl 1
	
	weight_modifier = {
		modifier = {
			factor = 5
			is_ai = yes
		}
		modifier = {
			factor = 2
			years_passed > 5
		}
		modifier = {
			factor = 2
			years_passed > 10
		}		
		modifier = {
			factor = 2
			years_passed > 15
		}		
		modifier = {
			factor = 2
			years_passed > 20
		}		
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_computing"
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_computing"
			}
		}
	}
}

tech_physics_lab_2 = {
	cost = @tier2cost2
	area = physics
	tier = 2
	category = { computing }
	prerequisites = { "tech_physics_lab_1" "tech_colonial_centralization"}
	is_reverse_engineerable = no
	weight = 0
		
	#unlock physics lab lvl 2
	
	weight_modifier = {
		modifier = {
			factor = 2
			years_passed > 25
		}
		modifier = {
			factor = 2
			years_passed > 30
		}		
		modifier = {
			factor = 2
			years_passed > 35
		}		
		modifier = {
			factor = 2
			years_passed > 40
		}
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_computing"
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_computing"
			}
		}
	}
}

tech_physics_lab_3 = {
	cost = @tier3cost3
	area = physics
	tier = 3
	category = { computing }
	prerequisites = { "tech_physics_lab_2" "tech_galactic_administration"}
	is_reverse_engineerable = no
	weight = 0
		
	# # unlock physics lab lvl 3
	
	weight_modifier = {
		modifier = {
			factor = 2
			years_passed > 25
		}
		modifier = {
			factor = 2
			years_passed > 30
		}		
		modifier = {
			factor = 2
			years_passed > 35
		}		
		modifier = {
			factor = 2
			years_passed > 40
		}	
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_computing"
			}
		}
	}
	
	ai_weight = {
		weight = 10
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_computing"
			}
		}
	}
}

#AI-Controlled Colony Ships
tech_cryostasis_1 = {
	cost = @tier1cost3
	area = physics
	tier = 1
	category = { computing }
	prerequisites = { "tech_administrative_ai" "tech_colonization_1" }
	is_reverse_engineerable = no
	weight = 0
	
	modifier = {
		planet_colony_development_speed_mult = 0.50
		shipsize_colonizer_build_cost_mult = -0.25
	}
}

#Self-Aware Colony Ships
tech_cryostasis_2 = {
	cost = @tier2cost1
	area = physics
	tier = 2
	category = { computing }
	prerequisites = { "tech_self_aware_logic" "tech_cryostasis_1" }
	is_reverse_engineerable = no
	weight = 0
	
	modifier = {
		planet_colony_development_speed_mult = 0.50
		shipsize_colonizer_build_cost_mult = -0.15
	}
}

tech_synchronized_defences = {
	area = physics
	tier = 1
	cost = @tier1cost2
	category = { computing }
	ai_update_type = military		
	prerequisites = { "tech_administrative_ai" }
	is_rare = yes
	is_reverse_engineerable = no
	weight = 0
	
	# unlocks orbital module - synchronized_defenses
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0.50
			NOR = {
				research_leader = {
					area = physics
					has_trait = "leader_trait_expertise_voidcraft"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_curator"
				}
			}
		}
		modifier = {
			factor = 0.75
			has_ethic = ethic_pacifist
		}
		modifier = {
			factor = 0.65
			has_ethic = ethic_fanatic_pacifist
		}
		modifier = {
			factor = 1.10
			has_ethic = ethic_militarist
		}
		modifier = {
			factor = 1.20
			has_ethic = ethic_fanatic_militarist
		}
	}
	
	ai_weight = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_computing"
			}
		}
	}	
}

# ##################
# Ship Reactors
# ##################

tech_fission_power = {
	area = physics
	cost = 0
	tier = 0
	category = { industry }
	ai_update_type = military
	is_reverse_engineerable = no
	weight = 0
}

tech_antimatter_power = {
	area = physics
	cost = @tier3cost1
	tier = 3
	category = { particles }
	ai_update_type = military
	is_reverse_engineerable = no
	weight = 0

	modifier = {
		max_energy = 1000
	}
	
	weight_modifier = {
		factor = 1.5
		modifier = {
			factor = 2
			has_technology = tech_spaceport_5
		}			
		modifier = {
			factor = 2
			has_technology = tech_spaceport_6
		}	
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
	
	ai_weight = {
		factor = 3 #important component
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

tech_zero_point_power = {
	area = physics
	cost = @tier3cost4
	tier = 3
	category = { particles }
	ai_update_type = military	
	prerequisites = { "tech_antimatter_power" }
	is_reverse_engineerable = no
	weight = 0
	
	modifier = {
		max_energy = 1000
	}
	
	weight_modifier = {
		factor = 1.5
		modifier = {
			factor = 2
			has_technology = tech_spaceport_6
		}	
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
	
	ai_weight = {
		factor = 3 #important component
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_industry"
			}
		}
	}
}

# ##################
# Shields and related techs
# ##################

tech_shields_5 = {
	area = physics
	cost = @tier3cost2
	tier = 3
	category = { field_manipulation }
	ai_update_type = all
	prerequisites = { "tech_shields_4" }
	weight = 0
	
	modifier = {
		planet_fortification_strength = 0.2
	}
	
	weight_modifier = {
		factor = 1.25
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
	
	ai_weight = {
		factor = 2 #good component
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
}

tech_shield_rechargers_1 = {
	area = physics
	cost = @tier2cost2
	tier = 2
	category = { field_manipulation }
	ai_update_type = all	
	prerequisites = { "tech_shields_3" }
	weight = @tier2weight2
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
	
	ai_weight = {
		factor = 2 #good component
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
}

tech_shield_recharge_aura_1 = {
	area = physics
	cost = @tier2cost4
	tier = 2
	category = { field_manipulation }
	ai_update_type = all	
	is_rare = yes
	prerequisites = { "tech_shields_4" }
	weight = @tier2weight4
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0.5
			NOR = {
				research_leader = {
					area = physics
					has_trait = "leader_trait_expertise_field_manipulation"
				}			
				research_leader = {
					area = physics
					has_trait = "leader_trait_curator"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_spark_of_genius"
				}
			}
		}
	}
	
	ai_weight = {
		factor = 2 #good component
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
	
		
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_SHIELD_RECHARGE_AURA_1_TITLE"
			desc = "TECH_UNLOCK_SHIELD_RECHARGE_AURA_1_DESC"
		}
	}
}

# ##################
# Psionic Shield
# ##################

tech_psionic_shield = {
	area = physics
	cost = @tier3cost4
	tier = 3
	category = { psionics }
	ai_update_type = all	
	is_rare = yes
	prerequisites = { "tech_psionic_theory" }
	weight = 0
	
	weight_modifier = {
		factor = 0
	}	
}

# ##################
# Sensors
# ##################

tech_sensors_2 = {
	area = physics
	cost = @tier1cost1
	tier = 1
	category = { voidcraft }
	ai_update_type = all
	weight = @tier1weight1
	
	modifier = {
		planet_sensor_range_mult = 0.5
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		factor = 1.25 #good component
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_SENSOR_2_TITLE"
			desc = "TECH_UNLOCK_SENSOR_2_DESC"
		}
	}
}

tech_sensors_3 = {
	area = physics
	cost = @tier2cost1
	tier = 2
	category = { voidcraft }
	ai_update_type = all	
	prerequisites = { "tech_sensors_2" }
	weight = 0
	
	modifier = {
		planet_sensor_range_mult = 0.5
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	ai_weight = {
		factor = 1.25 #good component
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_SENSOR_3_TITLE"
			desc = "TECH_UNLOCK_SENSOR_3_DESC"
		}
	}
}

tech_sensors_4 = {
	area = physics
	cost = @tier3cost1
	tier = 3
	category = { voidcraft }
	ai_update_type = all	
	prerequisites = { "tech_sensors_3" }
	weight = 0
		
	modifier = {
		planet_sensor_range_mult = 0.5
	}
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}	
	
	ai_weight = {
		factor = 1.25 #good component
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_voidcraft"
			}
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_SENSOR_4_TITLE"
			desc = "TECH_UNLOCK_SENSOR_4_DESC"
		}
	}
}

# ##################
# Power Plants
# ##################

# ##################
# FTL Drives 
# ##################

tech_wormhole_generation_1  = {
	cost = 0
	area = physics
	tier = 0
	category = { field_manipulation }
	weight = 0
	
	is_reverse_engineerable = no
	
	weight_modifier = {
		factor = 1
		modifier = {
			factor = 0
			OR = {
				has_technology = "tech_hyper_drive_1"
				has_technology = "tech_warp_drive_1"
			}
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_WORMHOLE_1_TITLE"
			desc = "TECH_UNLOCK_WORMHOLE_1_DESC"
		}
	}	
}

tech_wormhole_generation_2  = {
	cost = @tier2cost1
	area = physics
	tier = 2
	category = { field_manipulation }
	ai_update_type = all
	is_reverse_engineerable = no
	prerequisites = { "tech_wormhole_generation_1" }
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
	
	modifier = {
		ship_ftl_wormhole_speed_mult = 0.25
		ship_ftl_wormhole_range_mult = 0.5
	}
	
	ai_weight = {
		factor = 4 #FTL techs are good
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
}

tech_wormhole_generation_3  = {
	cost = @tier3cost1
	area = physics
	tier = 3
	category = { field_manipulation }
	ai_update_type = all
	is_reverse_engineerable = no	
	prerequisites = { "tech_wormhole_generation_2" }
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
	
	modifier = {
		ship_ftl_wormhole_speed_mult = 0.25
		ship_ftl_wormhole_range_mult = 0.5
	}
	
	ai_weight = {
		factor = 4 #FTL techs are good
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
}

tech_hyper_drive_1  = {
	cost = 0
	area = physics
	tier = 0
	category = { particles }
	weight = 0
	
	feature_flags = {
		hyperlanes
	}
	
	is_reverse_engineerable = no
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_HYPERDRIVE_1_TITLE"
			desc = "TECH_UNLOCK_HYPERDRIVE_1_DESC"
		}
	}
}

tech_hyper_drive_2 = {
	cost = @tier2cost1
	area = physics
	tier = 2
	category = { particles }
	ai_update_type = all
	is_reverse_engineerable = no
	prerequisites = { "tech_hyper_drive_1" }
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_particles"
			}
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_HYPERDRIVE_2_TITLE"
			desc = "TECH_UNLOCK_HYPERDRIVE_2_DESC"
		}
	}	
	
	ai_weight = {
		factor = 4 #FTL techs are good
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_particles"
			}
		}
	}
}

tech_hyper_drive_3 = {
	cost = @tier3cost1
	area = physics
	tier = 3
	category = { particles }
	ai_update_type = all
	is_reverse_engineerable = no
	prerequisites = { "tech_hyper_drive_2" }
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_particles"
			}
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_HYPERDRIVE_3_TITLE"
			desc = "TECH_UNLOCK_HYPERDRIVE_3_DESC"
		}
	}
	
	ai_weight = {
		factor = 4 #FTL techs are good
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_particles"
			}
		}
	}
}

tech_hyperlane_mapping = {
	cost = @tier2cost1
	area = physics
	tier = 2
	category = { particles }
	
	is_rare = yes
	weight = 0
	
	feature_flags = {
		hyperlanes
	}
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0.20
			NOR = {
				research_leader = {
					area = physics
					has_trait = "leader_trait_expertise_particles"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_curator"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_maniacal"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_spark_of_genius"
				}
			}
		}		
		#maybe add factor if discovered debris with hyperlane drive
		modifier = {
			factor = 0
			has_technology = "tech_hyper_drive_1"
		}
	}
	
	ai_weight = {
		factor = 0	#AI doesn't need it :O
	}
}

tech_warp_drive_1 = {
	cost = 0
	area = physics
	tier = 0
	category = { field_manipulation }
	weight = 0
	
	is_reverse_engineerable = no
	
	weight_modifier = {
		factor = 0
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_WARPDRIVE_1_TITLE"
			desc = "TECH_UNLOCK_WARPDRIVE_1_DESC"
		}
	}
}

tech_warp_drive_2 = {
	cost = @tier2cost1
	area = physics
	tier = 2
	category = { field_manipulation }
	ai_update_type = all
	is_reverse_engineerable = no	
	prerequisites = { "tech_warp_drive_1" }
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_WARPDRIVE_2_TITLE"
			desc = "TECH_UNLOCK_WARPDRIVE_2_DESC"
		}
	}
	
	ai_weight = {
		factor = 4 #FTL techs are good
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
}

tech_warp_drive_3 = {
	cost = @tier3cost1
	area = physics
	tier = 3
	category = { field_manipulation }
	ai_update_type = all
	is_reverse_engineerable = no	
	prerequisites = { "tech_warp_drive_2" }
	weight = 0
	
	weight_modifier = {
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_WARPDRIVE_3_TITLE"
			desc = "TECH_UNLOCK_WARPDRIVE_3_DESC"
		}
	}

	ai_weight = {
		factor = 4 #FTL techs are good
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
}

tech_jump_drive_1 = {
	cost = @tier3cost1
	area = physics
	tier = 3
	category = { field_manipulation }
	ai_update_type = all
	is_reverse_engineerable = no
	is_rare = yes
	is_dangerous = yes
	weight = 0
	prerequisites = { "tech_zero_point_power" }
	
	weight_modifier = {
		factor = 0.1
		modifier = {
			factor = 0.1
			NOR = {
				research_leader = {
					area = physics
					has_trait = "leader_trait_expertise_field_manipulation"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_curator"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_spark_of_genius"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_maniacal"
				}
			}		
		}
		
		modifier = {
			factor = 0.5
			NOT = { has_technology = tech_wormhole_generation_3 }
		}
	}
	
	ai_weight = {
		factor = 20 #FTL techs are good
	}
	
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_JUMPDRIVE_1_TITLE"
			desc = "TECH_UNLOCK_JUMPDRIVE_1_DESC"
		}
	}
}

# ##################
# Debuff auras
# ##################


tech_aura_quantum_destabilizer = {
	area = physics
	cost = @tier2cost4
	tier = 2
	category = { field_manipulation }
	ai_update_type = military	
	is_rare = yes
	prerequisites = { "tech_shields_3" }
	weight = 0
	is_reverse_engineerable = no
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0.20
			NOR = {
				research_leader = {
					area = physics
					has_trait = "leader_trait_expertise_field_manipulation"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_curator"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_spark_of_genius"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_maniacal"
				}
			}		
		}
	}
	
	ai_weight = {
		factor = 2 #good component
		modifier = {
			factor = 1.25
			research_leader = {
				area = physics
				has_trait = "leader_trait_expertise_field_manipulation"
			}
		}
	}
	
		
	prereqfor_desc = {
		component = {
			title = "TECH_UNLOCK_QUANTUM_DESTABILIZER_AURA_1_TITLE"
			desc = "TECH_UNLOCK_QUANTUM_DESTABILIZER_AURA_1_DESC"
		}
	}
}

# ##################
# Ascension Perk
# ##################

tech_ascension_theory = {
	area = physics
	cost = 8000
	tier = 4
	category = { computing }
	ai_update_type = all
	is_rare = yes
	weight = 0
	is_reverse_engineerable = no
	
	modifier = {
		ascension_perks_add = 1
	}
	
	weight_modifier = {
		factor = 0.5
		modifier = {
			factor = 0
			NOT = { host_has_dlc = "Utopia" }
		}
		modifier = {
			factor = 0.20
			NOR = {
				has_modifier = "curator_insight"
				research_leader = {
					area = physics
					has_trait = "leader_trait_curator"
				}
				research_leader = {
					area = physics
					has_trait = "leader_trait_expertise_computing"
				}			
			}
		}
	}
	
	ai_weight = {
		weight = 9999
	}
}