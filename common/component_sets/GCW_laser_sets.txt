#Blasters

component_set = { key = "BLASTER_G_1" icon = "GFX_ship_part_ship_blaster_g_1" icon_frame = 1 }
component_set = { key = "BLASTER_G_2" icon = "GFX_ship_part_ship_blaster_g_2" icon_frame = 1 }
component_set = { key = "BLASTER_G_3" icon = "GFX_ship_part_ship_blaster_g_3" icon_frame = 1 }
component_set = { key = "BLASTER_G_4" icon = "GFX_ship_part_ship_blaster_g_4" icon_frame = 1 }

component_set = { key = "BLASTER_R_1" icon = "GFX_ship_part_ship_blaster_r_1" icon_frame = 1 }
component_set = { key = "BLASTER_R_2" icon = "GFX_ship_part_ship_blaster_r_2" icon_frame = 1 }
component_set = { key = "BLASTER_R_3" icon = "GFX_ship_part_ship_blaster_r_3" icon_frame = 1 }
component_set = { key = "BLASTER_R_4" icon = "GFX_ship_part_ship_blaster_r_4" icon_frame = 1 }

component_set = { key = "BLASTER_B_1" icon = "GFX_ship_part_ship_blaster_b_1" icon_frame = 1 }
component_set = { key = "BLASTER_B_2" icon = "GFX_ship_part_ship_blaster_b_2" icon_frame = 1 }
component_set = { key = "BLASTER_B_3" icon = "GFX_ship_part_ship_blaster_b_3" icon_frame = 1 }
component_set = { key = "BLASTER_B_4" icon = "GFX_ship_part_ship_blaster_b_4" icon_frame = 1 }

component_set = { key = "BLASTER_P_1" icon = "GFX_ship_part_ship_blaster_p_1" icon_frame = 1 }
component_set = { key = "BLASTER_P_2" icon = "GFX_ship_part_ship_blaster_p_2" icon_frame = 1 }
component_set = { key = "BLASTER_P_3" icon = "GFX_ship_part_ship_blaster_p_3" icon_frame = 1 }
component_set = { key = "BLASTER_P_4" icon = "GFX_ship_part_ship_blaster_p_4" icon_frame = 1 }

component_set = { key = "BLASTER_Y_1" icon = "GFX_ship_part_ship_blaster_y_1" icon_frame = 1 }
component_set = { key = "BLASTER_Y_2" icon = "GFX_ship_part_ship_blaster_y_2" icon_frame = 1 }
component_set = { key = "BLASTER_Y_3" icon = "GFX_ship_part_ship_blaster_y_3" icon_frame = 1 }
component_set = { key = "BLASTER_Y_4" icon = "GFX_ship_part_ship_blaster_y_4" icon_frame = 1 }

#Ion Cannons

component_set = { key = "ION_CANNON_1" icon = "GFX_ship_part_ion_cannon_1" icon_frame = 1 }
component_set = { key = "ION_CANNON_2" icon = "GFX_ship_part_ion_cannon_2" icon_frame = 1 }
component_set = { key = "ION_CANNON_3" icon = "GFX_ship_part_ion_cannon_3" icon_frame = 1 }
component_set = { key = "ION_CANNON_4" icon = "GFX_ship_part_ion_cannon_4" icon_frame = 1 }
component_set = { key = "ION_CANNON_5" icon = "GFX_ship_part_ion_cannon_5" icon_frame = 1 }

component_set = { key = "HEAVY_ION_CANNON_1" icon = "GFX_ship_part_heavy_ion_cannon_1" icon_frame = 1 }
component_set = { key = "HEAVY_ION_CANNON_2" icon = "GFX_ship_part_heavy_ion_cannon_2" icon_frame = 1 }
component_set = { key = "HEAVY_ION_CANNON_3" icon = "GFX_ship_part_heavy_ion_cannon_3" icon_frame = 1 }
component_set = { key = "HEAVY_ION_CANNON_4" icon = "GFX_ship_part_heavy_ion_cannon_4" icon_frame = 1 }
component_set = { key = "HEAVY_ION_CANNON_5" icon = "GFX_ship_part_heavy_ion_cannon_5" icon_frame = 1 }

#Turbolasers

component_set = { key = "TURBOLASER_G_1" icon = "GFX_ship_part_turbo_g_1" icon_frame = 1 }
component_set = { key = "TURBOLASER_G_2" icon = "GFX_ship_part_turbo_g_2" icon_frame = 1 }
component_set = { key = "TURBOLASER_G_3" icon = "GFX_ship_part_turbo_g_3" icon_frame = 1 }
component_set = { key = "TURBOLASER_G_4" icon = "GFX_ship_part_turbo_g_4" icon_frame = 1 }
component_set = { key = "TURBOLASER_G_5" icon = "GFX_ship_part_turbo_g_5" icon_frame = 1 }

component_set = { key = "TURBOLASER_R_1" icon = "GFX_ship_part_turbo_r_1" icon_frame = 1 }
component_set = { key = "TURBOLASER_R_2" icon = "GFX_ship_part_turbo_r_2" icon_frame = 1 }
component_set = { key = "TURBOLASER_R_3" icon = "GFX_ship_part_turbo_r_3" icon_frame = 1 }
component_set = { key = "TURBOLASER_R_4" icon = "GFX_ship_part_turbo_r_4" icon_frame = 1 }
component_set = { key = "TURBOLASER_R_5" icon = "GFX_ship_part_turbo_r_5" icon_frame = 1 }

component_set = { key = "TURBOLASER_B_1" icon = "GFX_ship_part_turbo_b_1" icon_frame = 1 }
component_set = { key = "TURBOLASER_B_2" icon = "GFX_ship_part_turbo_b_2" icon_frame = 1 }
component_set = { key = "TURBOLASER_B_3" icon = "GFX_ship_part_turbo_b_3" icon_frame = 1 }
component_set = { key = "TURBOLASER_B_4" icon = "GFX_ship_part_turbo_b_4" icon_frame = 1 }
component_set = { key = "TURBOLASER_B_5" icon = "GFX_ship_part_turbo_b_5" icon_frame = 1 }

component_set = { key = "TURBOLASER_P_1" icon = "GFX_ship_part_turbo_p_1" icon_frame = 1 }
component_set = { key = "TURBOLASER_P_2" icon = "GFX_ship_part_turbo_p_2" icon_frame = 1 }
component_set = { key = "TURBOLASER_P_3" icon = "GFX_ship_part_turbo_p_3" icon_frame = 1 }
component_set = { key = "TURBOLASER_P_4" icon = "GFX_ship_part_turbo_p_4" icon_frame = 1 }
component_set = { key = "TURBOLASER_P_5" icon = "GFX_ship_part_turbo_p_5" icon_frame = 1 }

component_set = { key = "TURBOLASER_Y_1" icon = "GFX_ship_part_turbo_y_1" icon_frame = 1 }
component_set = { key = "TURBOLASER_Y_2" icon = "GFX_ship_part_turbo_y_2" icon_frame = 1 }
component_set = { key = "TURBOLASER_Y_3" icon = "GFX_ship_part_turbo_y_3" icon_frame = 1 }
component_set = { key = "TURBOLASER_Y_4" icon = "GFX_ship_part_turbo_y_4" icon_frame = 1 }
component_set = { key = "TURBOLASER_Y_5" icon = "GFX_ship_part_turbo_y_5" icon_frame = 1 }

#Heavy Turbolasers

component_set = { key = "HEAVY_TURBOLASER_G_1" icon = "GFX_ship_part_heavy_turbo_g_1" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_G_2" icon = "GFX_ship_part_heavy_turbo_g_2" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_G_3" icon = "GFX_ship_part_heavy_turbo_g_3" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_G_4" icon = "GFX_ship_part_heavy_turbo_g_4" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_G_5" icon = "GFX_ship_part_heavy_turbo_g_5" icon_frame = 1 }

component_set = { key = "HEAVY_TURBOLASER_R_1" icon = "GFX_ship_part_heavy_turbo_r_1" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_R_2" icon = "GFX_ship_part_heavy_turbo_r_2" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_R_3" icon = "GFX_ship_part_heavy_turbo_r_3" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_R_4" icon = "GFX_ship_part_heavy_turbo_r_4" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_R_5" icon = "GFX_ship_part_heavy_turbo_r_5" icon_frame = 1 }

component_set = { key = "HEAVY_TURBOLASER_B_1" icon = "GFX_ship_part_heavy_turbo_b_1" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_B_2" icon = "GFX_ship_part_heavy_turbo_b_2" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_B_3" icon = "GFX_ship_part_heavy_turbo_b_3" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_B_4" icon = "GFX_ship_part_heavy_turbo_b_4" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_B_5" icon = "GFX_ship_part_heavy_turbo_b_5" icon_frame = 1 }

component_set = { key = "HEAVY_TURBOLASER_P_1" icon = "GFX_ship_part_heavy_turbo_p_1" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_P_2" icon = "GFX_ship_part_heavy_turbo_p_2" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_P_3" icon = "GFX_ship_part_heavy_turbo_p_3" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_P_4" icon = "GFX_ship_part_heavy_turbo_p_4" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_P_5" icon = "GFX_ship_part_heavy_turbo_p_5" icon_frame = 1 }

component_set = { key = "HEAVY_TURBOLASER_Y_1" icon = "GFX_ship_part_heavy_turbo_y_1" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_Y_2" icon = "GFX_ship_part_heavy_turbo_y_2" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_Y_3" icon = "GFX_ship_part_heavy_turbo_y_3" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_Y_4" icon = "GFX_ship_part_heavy_turbo_y_4" icon_frame = 1 }
component_set = { key = "HEAVY_TURBOLASER_Y_5" icon = "GFX_ship_part_heavy_turbo_y_5" icon_frame = 1 }

#XL Turbolasers

component_set = { key = "XL_TURBOLASER_G_1" icon = "GFX_ship_part_xl_turbo_g_1" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_G_2" icon = "GFX_ship_part_xl_turbo_g_2" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_G_3" icon = "GFX_ship_part_xl_turbo_g_3" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_G_4" icon = "GFX_ship_part_xl_turbo_g_4" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_G_5" icon = "GFX_ship_part_xl_turbo_g_5" icon_frame = 1 }

component_set = { key = "XL_TURBOLASER_R_1" icon = "GFX_ship_part_xl_turbo_r_1" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_R_2" icon = "GFX_ship_part_xl_turbo_r_2" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_R_3" icon = "GFX_ship_part_xl_turbo_r_3" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_R_4" icon = "GFX_ship_part_xl_turbo_r_4" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_R_5" icon = "GFX_ship_part_xl_turbo_r_5" icon_frame = 1 }

component_set = { key = "XL_TURBOLASER_B_1" icon = "GFX_ship_part_xl_turbo_b_1" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_B_2" icon = "GFX_ship_part_xl_turbo_b_2" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_B_3" icon = "GFX_ship_part_xl_turbo_b_3" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_B_4" icon = "GFX_ship_part_xl_turbo_b_4" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_B_5" icon = "GFX_ship_part_xl_turbo_b_5" icon_frame = 1 }

component_set = { key = "XL_TURBOLASER_P_1" icon = "GFX_ship_part_xl_turbo_p_1" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_P_2" icon = "GFX_ship_part_xl_turbo_p_2" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_P_3" icon = "GFX_ship_part_xl_turbo_p_3" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_P_4" icon = "GFX_ship_part_xl_turbo_p_4" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_P_5" icon = "GFX_ship_part_xl_turbo_p_5" icon_frame = 1 }

component_set = { key = "XL_TURBOLASER_Y_1" icon = "GFX_ship_part_xl_turbo_y_1" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_Y_2" icon = "GFX_ship_part_xl_turbo_y_2" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_Y_3" icon = "GFX_ship_part_xl_turbo_y_3" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_Y_4" icon = "GFX_ship_part_xl_turbo_y_4" icon_frame = 1 }
component_set = { key = "XL_TURBOLASER_Y_5" icon = "GFX_ship_part_xl_turbo_y_5" icon_frame = 1 }