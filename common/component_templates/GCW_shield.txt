## CORVETTES ##

# Corvette Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_CV"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	upgrades_to = ""	
	size_restriction = { gcwcorvette }
	modifier = { ship_shield_hp_add = 1000 ship_shield_regen_add = 0.5  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}

## FRIGATES ##

# Frigate Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_FR"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	upgrades_to = ""	
	size_restriction = { gcwfrigate }
	modifier = { ship_shield_hp_add = 1500 ship_shield_regen_add = 0.5  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}

## ADVANCED FRIGATES ##

# Advanced Frigate Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_AF"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	upgrades_to = ""
	size_restriction = { adv_frigate }	
	modifier = { ship_shield_hp_add = 1750 ship_shield_regen_add = 0.5  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}

## CRUISERS ##

# Cruiser Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_CR"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	upgrades_to = ""
	size_restriction = { gcwcruiser }
	modifier = { ship_shield_hp_add = 2500 ship_shield_regen_add = 1  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}

## HEAVY CRUISERS ##

# Heavy Cruiser Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_HC"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	upgrades_to = ""	
	size_restriction = { heavy_cruiser }	
	modifier = { ship_shield_hp_add = 5000 ship_shield_regen_add = 1  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}
	
## STAR DESTROYERS ##

# Star Destroyer Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_SD"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	#upgrades_to = "   "	
	size_restriction = { star_destroyer }
	modifier = { ship_shield_hp_add = 7500 ship_shield_regen_add = 2  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}
	
## ADVANCED STAR DESTROYERS ##

# Advanced Star Destroyer Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_AS"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	upgrades_to = ""	
	size_restriction = { adv_star_destroyer }	
	modifier = { ship_shield_hp_add = 8500 ship_shield_regen_add = 2  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}
	
## BATTLECRUISERS ##

# Battlecruiser Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_BC"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	upgrades_to = ""	
	size_restriction = { battlecruiser }	
	modifier = { ship_shield_hp_add = 12500 ship_shield_regen_add = 3  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}

## DREADNOUGHTS ##

# Dreadnought Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_DN"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	upgrades_to = ""
	size_restriction = { dreadnought }	
	modifier = { ship_shield_hp_add = 50000 ship_shield_regen_add = 5  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}
	
## INTERDICTORS ##

# Interdictor Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_IN"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	upgrades_to = ""
	size_restriction = { interdictor }	
	modifier = { ship_shield_hp_add = 1500 ship_shield_regen_add = 0.5  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}

## HEAVY INTERDICTORS ##

# Heavy Interdictor Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_HI"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	upgrades_to = ""
	size_restriction = { heavy_interdictor }	
	modifier = { ship_shield_hp_add = 2500 ship_shield_regen_add = 1  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}

## SUPER WEAPONS ##

# Super Weapon Basic Shields
	utility_component_template = {	
	can_recharge_shield = yes
	key = "GCW_Ship_Shield_0_SW"	
	size = small	
	icon = "GFX_ship_part_shield_1"	
	icon_frame = 1	
	power = 0	
	cost = 40	 
	upgrades_to = ""	
	size_restriction = { super_weapon }
	modifier = { ship_shield_hp_add = 100000 ship_shield_regen_add = 10  }	
	ai_weight = { weight = 100 }		
	#prerequisites = { "tech_shields_1" }	
	component_set = "GCW_shield"	
	}