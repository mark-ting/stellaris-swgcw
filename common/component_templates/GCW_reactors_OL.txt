#############
#	POWER	#
#############

@power_OL1 = 60
@power_OL2 = 80
@power_OL3 = 100
@power_OL4 = 120
@power_OL5 = 140

#############
#	COST	#
#############

@cost_OL1 = 20
@cost_OL2 = 25
@cost_OL3 = 30
@cost_OL4 = 35
@cost_OL5 = 40

# OL Fusion Reactors

utility_component_template = {
	key = "SMALL_OL_FUSION_REACTOR"
	size = small
	icon = "GFX_ship_part_fusion_OL"
	icon_frame = 1
	power = @power_OL1
	cost = @cost_OL1
	ai_weight = { weight = 1 }

	ship_modifier = { ship_windup_mult = 0.10 }
	
	prerequisites = { "tech_fusion_power" }
	component_set = "OL_FUSION_REACTOR"
	upgrades_to = "SMALL_OL_COLD_FUSION_REACTOR"
}

utility_component_template = {
	key = "MEDIUM_OL_FUSION_REACTOR"
	size = medium
	icon = "GFX_ship_part_fusion_OL"
	icon_frame = 1
	power = @power_OL1
	cost = @cost_OL1
	ai_weight = { weight = 1 }

	ship_modifier = { ship_windup_mult = 0.10 }
	
	prerequisites = { "tech_fusion_power" }
	component_set = "OL_FUSION_REACTOR"
	upgrades_to = "MEDIUM_OL_COLD_FUSION_REACTOR"
}

utility_component_template = {
	key = "LARGE_OL_FUSION_REACTOR"
	size = large
	icon = "GFX_ship_part_fusion_OL"
	icon_frame = 1
	power = @power_OL1
	cost = @cost_OL1
	ai_weight = { weight = 1 }

	ship_modifier = { ship_windup_mult = 0.10 }
	
	prerequisites = { "tech_fusion_power" }
	component_set = "OL_FUSION_REACTOR"
	upgrades_to = "LARGE_OL_COLD_FUSION_REACTOR"
}

utility_component_template = {
	key = "AUX_OL_FUSION_REACTOR"
	size = aux
	icon = "GFX_ship_part_fusion_OL"
	icon_frame = 1
	power = @power_OL1
	cost = @cost_OL1
	ai_weight = { weight = 1 }

	ship_modifier = { ship_windup_mult = 0.10 }
	
	prerequisites = { "tech_fusion_power" }
	component_set = "OL_FUSION_REACTOR"
	upgrades_to = "AUX_OL_COLD_FUSION_REACTOR"
}


