### GCW HYPERDRIVES - SUPERWEAPONS
utility_component_template = {
	key = "SW_DS_HYPERDRIVE"
	size = small
	icon = "GFX_ship_part_hyper_drive_1"
	icon_frame = 1
	power = 150
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = 0.3
	}
	
	ai_weight = {
		weight = 1
	}
	
	size_restriction = { superweapon }
	component_set = "ftl_components"
}



### GCW HYPERDRIVES - CIVILIAN
utility_component_template = {
	key = "SW_CIV_HYPERDRIVE_1"
	size = small
	icon = "GFX_ship_part_hyper_drive_1"
	icon_frame = 1
	power = 75
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = -0.45
	}
	
	ai_weight = {
		weight = 1
	}
	
	class_restriction = { shipclass_constructor shipclass_colonizer shipclass_science_ship shipclass_transport }
	prerequisites = { "tech_hyperdrive_1" }
	component_set = "ftl_components"
	upgrades_to = "SW_CIV_HYPERDRIVE_2"
}

utility_component_template = {
	key = "SW_CIV_HYPERDRIVE_2"
	size = small
	icon = "GFX_ship_part_hyper_drive_2"
	icon_frame = 1
	power = 100
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = -0.60
	}
	
	ai_weight = {
		weight = 2
	}
	
	class_restriction = { shipclass_constructor shipclass_colonizer shipclass_science_ship shipclass_transport }
	prerequisites = { "tech_hyperdrive_2" }
	component_set = "ftl_components"
	upgrades_to = "SW_CIV_HYPERDRIVE_3"
}

utility_component_template = {
	key = "SW_CIV_HYPERDRIVE_3"
	size = small
	icon = "GFX_ship_part_hyper_drive_3"
	icon_frame = 1
	power = 150
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = -0.75
	}
	
	ai_weight = {
		weight = 3
	}
	
	class_restriction = { shipclass_constructor shipclass_colonizer shipclass_science_ship shipclass_transport }
	prerequisites = { "tech_hyperdrive_3" }
	component_set = "ftl_components"
}



### GCW HYPERDRIVES - COR/FRIG/AFRIG/INT
utility_component_template = {
	key = "SW_CV_HYPERDRIVE_1"
	size = small
	icon = "GFX_ship_part_hyper_drive_1"
	icon_frame = 1
	power = 75
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = -0.45
	}
	
	ai_weight = {
		weight = 1
	}
	
	size_restriction = { gcwcorvette gcwfrigate adv_frigate interdictor }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_1" }
	component_set = "ftl_components"
	upgrades_to = "SW_CV_HYPERDRIVE_2"
}

utility_component_template = {
	key = "SW_CV_HYPERDRIVE_2"
	size = small
	icon = "GFX_ship_part_hyper_drive_2"
	icon_frame = 1
	power = 100
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = -0.60
	}
	
	ai_weight = {
		weight = 2
	}
	
	size_restriction = { gcwcorvette gcwfrigate adv_frigate interdictor }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_2" }
	component_set = "ftl_components"
	upgrades_to = "SW_CV_HYPERDRIVE_3"
}

utility_component_template = {
	key = "SW_CV_HYPERDRIVE_3"
	size = small
	icon = "GFX_ship_part_hyper_drive_3"
	icon_frame = 1
	power = 150
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = -0.75
	}
	
	ai_weight = {
		weight = 3
	}
	
	size_restriction = { gcwcorvette gcwfrigate adv_frigate interdictor }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_3" }
	component_set = "ftl_components"
}



### GCW HYPERDRIVES - CR/HC/HI
utility_component_template = {
	key = "SW_CR_HYPERDRIVE_1"
	size = small
	icon = "GFX_ship_part_hyper_drive_1"
	icon_frame = 1
	power = 75
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = -0.15
	}
	
	ai_weight = {
		weight = 1
	}
	
	size_restriction = { gcwcruiser heavy_cruiser heavy_interdictor }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_1" }
	component_set = "ftl_components"
	upgrades_to = "SW_CR_HYPERDRIVE_2"
}

utility_component_template = {
	key = "SW_CR_HYPERDRIVE_2"
	size = small
	icon = "GFX_ship_part_hyper_drive_2"
	icon_frame = 1
	power = 100
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = -0.30
	}
	
	ai_weight = {
		weight = 2
	}
	
	size_restriction = { gcwcruiser heavy_cruiser heavy_interdictor }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_2" }
	component_set = "ftl_components"
	upgrades_to = "SW_CR_HYPERDRIVE_3"
}

utility_component_template = {
	key = "SW_CR_HYPERDRIVE_3"
	size = small
	icon = "GFX_ship_part_hyper_drive_3"
	icon_frame = 1
	power = 150
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = -0.30
	}
	
	ai_weight = {
		weight = 3
	}
	
	size_restriction = { gcwcruiser heavy_cruiser heavy_interdictor }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_3" }
	component_set = "ftl_components"
}



### GCW HYPERDRIVES - SD/ASD/BC
utility_component_template = {
	key = "SW_SD_HYPERDRIVE_1"
	size = small
	icon = "GFX_ship_part_hyper_drive_1"
	icon_frame = 1
	power = 75
	cost = 0
	ftl = hyperdrive
	
	ai_weight = {
		weight = 1
	}
	
	size_restriction = { star_destroyer adv_star_destroyer battlecruiser }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_1" }
	component_set = "ftl_components"
	upgrades_to = "SW_SD_HYPERDRIVE_2"
}

utility_component_template = {
	key = "SW_SD_HYPERDRIVE_2"
	size = small
	icon = "GFX_ship_part_hyper_drive_2"
	icon_frame = 1
	power = 100
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = -0.15
	}
	
	ai_weight = {
		weight = 2
	}
	
	size_restriction = { star_destroyer adv_star_destroyer battlecruiser }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_2" }
	component_set = "ftl_components"
	upgrades_to = "SW_SD_HYPERDRIVE_3"
}

utility_component_template = {
	key = "SW_SD_HYPERDRIVE_3"
	size = small
	icon = "GFX_ship_part_hyper_drive_3"
	icon_frame = 1
	power = 150
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = -0.30
	}
	
	ai_weight = {
		weight = 3
	}
	
	size_restriction = { star_destroyer adv_star_destroyer battlecruiser }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_3" }
	component_set = "ftl_components"
}



### GCW HYPERDRIVES - DN
utility_component_template = {
	key = "SW_DN_HYPERDRIVE_1"
	size = small
	icon = "GFX_ship_part_hyper_drive_1"
	icon_frame = 1
	power = 75
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = 0.30
	}
	
	ai_weight = {
		weight = 1
	}
	
	size_restriction = { dreadnought }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_1" }
	component_set = "ftl_components"
	upgrades_to = "SW_DN_HYPERDRIVE_2"
}

utility_component_template = {
	key = "SW_DN_HYPERDRIVE_2"
	size = small
	icon = "GFX_ship_part_hyper_drive_2"
	icon_frame = 1
	power = 100
	cost = 0
	ftl = hyperdrive
	ship_modifier = {
		ship_windup_mult = 0.15
	}
	
	ai_weight = {
		weight = 2
	}
	
	size_restriction = { dreadnought }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_2" }
	component_set = "ftl_components"
	upgrades_to = "SW_DN_HYPERDRIVE_3"
}

utility_component_template = {
	key = "SW_DN_HYPERDRIVE_3"
	size = small
	icon = "GFX_ship_part_hyper_drive_3"
	icon_frame = 1
	power = 150
	cost = 0
	ftl = hyperdrive
	
	ai_weight = {
		weight = 3
	}
	
	size_restriction = { dreadnought }
	class_restriction = { shipclass_military }
	prerequisites = { "tech_hyperdrive_3" }
	component_set = "ftl_components"
}