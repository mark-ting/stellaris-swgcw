##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# monmotha
	monmotha_male_01 = { entity = "portrait_monmotha_female_01_entity" clothes_selector = "monmotha_female_clothes_01" hair_selector = "monmotha_female_hair_01" greeting_sound = "human_female_greetings_01"
		character_textures = { "gfx/models/portraits/human/human_female_body_03.dds" }
	}
}

portrait_groups = {
	monmotha = {
		default = monmotha_male_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					monmotha_male_01
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					monmotha_male_01
				}
			}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					monmotha_male_01
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					monmotha_male_01
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					monmotha_male_01
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					monmotha_male_01
				}
			}
		}

			
		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					monmotha_male_01
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					monmotha_male_01
				}
			}
		}
	}
}