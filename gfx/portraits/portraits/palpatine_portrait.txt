##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# palpatine
	palpatine_male_01 = { entity = "portrait_palpatine_male_01_entity" clothes_selector = "palpatine_male_clothes_01" hair_selector = "palpatine_male_hair_01" greeting_sound = "palpatine"
		character_textures = { "gfx/models/portraits/imperial/human_palpatine_body_01.dds" }
	}
}

portrait_groups = {
	palpatine = {
		default = palpatine_male_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					palpatine_male_01
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					palpatine_male_01
				}
			}
			#set = {
			#	trigger = { ... }
			#	portraits = { ... }
			#	#using "set =" instead of "add" will first clear any portraits already added
			#}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					palpatine_male_01
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					palpatine_male_01
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					palpatine_male_01
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					palpatine_male_01
				}
			}
		}

			
		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					palpatine_male_01
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					palpatine_male_01
				}
			}
		}
	}
}