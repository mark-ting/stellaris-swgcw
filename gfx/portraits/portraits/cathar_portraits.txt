##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# cathar
	cathar_female_01 = { entity = "portrait_cathar_female_01_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_female_hair_01" greeting_sound = "human_female_greetings_03"
		character_textures = { "gfx/models/portraits/cathar/cathar_female_body_01.dds" }
	}
	cathar_female_02 = { entity = "portrait_cathar_female_02_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_female_hair_01" greeting_sound = "human_female_greetings_04"
		character_textures = { "gfx/models/portraits/cathar/cathar_female_body_02.dds" }
	}
	cathar_female_03 = { entity = "portrait_cathar_female_03_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_female_hair_01" greeting_sound = "human_female_greetings_05"
		character_textures = { "gfx/models/portraits/cathar/cathar_female_body_03.dds" }
	}
	cathar_female_04 = { entity = "portrait_cathar_female_04_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_female_hair_01" greeting_sound = "human_female_greetings_01"
		character_textures = { "gfx/models/portraits/cathar/cathar_female_body_04.dds" }
	}
	cathar_female_05 = { entity = "portrait_cathar_female_05_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_female_hair_01" greeting_sound = "human_female_greetings_02"
		character_textures = { "gfx/models/portraits/cathar/cathar_female_body_05.dds" }
	}
	cathar_female_06 = { entity = "portrait_cathar_female_06_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_female_hair_01" greeting_sound = "human_female_greetings_03"
		character_textures = { "gfx/models/portraits/cathar/cathar_female_body_06.dds" }
	}
	cathar_female_07 = { entity = "portrait_cathar_female_07_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_female_hair_01" greeting_sound = "human_female_greetings_04"
		character_textures = { "gfx/models/portraits/cathar/cathar_female_body_07.dds" }
	}
	cathar_female_08 = { entity = "portrait_cathar_female_08_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_female_hair_01" greeting_sound = "human_female_greetings_05"
		character_textures = { "gfx/models/portraits/cathar/cathar_female_body_08.dds" }
	}
	cathar_male_01 = { entity = "portrait_cathar_male_01_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_male_hair_01" greeting_sound = "human_male_greetings_03"
		character_textures = { "gfx/models/portraits/cathar/cathar_male_body_01.dds" }
	}	
	cathar_male_02 = { entity = "portrait_cathar_male_02_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_male_hair_01" greeting_sound = "human_male_greetings_04"
		character_textures = { "gfx/models/portraits/cathar/cathar_male_body_02.dds" }
	}
	cathar_male_03 = { entity = "portrait_cathar_male_03_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_male_hair_01" greeting_sound = "human_male_greetings_05"
		character_textures = { "gfx/models/portraits/cathar/cathar_male_body_03.dds" }
	}
	cathar_male_04 = { entity = "portrait_cathar_male_04_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_male_hair_01" greeting_sound = "human_male_greetings_01"
		character_textures = { "gfx/models/portraits/cathar/cathar_male_body_04.dds" }
	}
	cathar_male_05 = { entity = "portrait_cathar_male_05_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_male_hair_01" greeting_sound = "human_male_greetings_02"
		character_textures = { "gfx/models/portraits/cathar/cathar_male_body_05.dds" }
	}
	cathar_male_06 = { entity = "portrait_cathar_male_06_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_male_hair_01" greeting_sound = "human_male_greetings_03"
		character_textures = { "gfx/models/portraits/cathar/cathar_male_body_06.dds" }
	}
	cathar_male_07 = { entity = "portrait_cathar_male_07_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_male_hair_01" greeting_sound = "human_male_greetings_04"
		character_textures = { "gfx/models/portraits/cathar/cathar_male_body_07.dds" }
	}
	cathar_male_08 = { entity = "portrait_cathar_male_08_entity" clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "cathar_male_hair_01" greeting_sound = "human_male_greetings_05"
		character_textures = { "gfx/models/portraits/cathar/cathar_male_body_08.dds" }
	}
}

portrait_groups = {
	cathar = {
		default = cathar_female_07
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					cathar_male_01
					cathar_male_02
					cathar_male_03
					cathar_male_04
					cathar_male_05
					cathar_male_06
					cathar_male_07
					cathar_male_08
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					cathar_female_01
					cathar_female_02
					cathar_female_03
					cathar_female_04					
					cathar_female_05					
					cathar_female_06					
					cathar_female_07					
					cathar_female_08
				}
			}
			#set = {
			#	trigger = { ... }
			#	portraits = { ... }
			#	#using "set =" instead of "add" will first clear any portraits already added
			#}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					cathar_female_01
					cathar_female_02
					cathar_female_03					
					cathar_female_04					
					cathar_female_05					
					cathar_female_06					
					cathar_female_07					
					cathar_female_08
					cathar_male_01
					cathar_male_02
					cathar_male_03					
					cathar_male_04
					cathar_male_05
					cathar_male_06
					cathar_male_07
					cathar_male_08
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					cathar_female_01
					cathar_female_02
					cathar_female_03				
					cathar_female_04					
					cathar_female_05					
					cathar_female_06					
					cathar_female_07					
					cathar_female_08
					cathar_male_01
					cathar_male_02
					cathar_male_03					
					cathar_male_04
					cathar_male_05
					cathar_male_06
					cathar_male_07
					cathar_male_08
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					cathar_female_01
					cathar_female_02
					cathar_female_03					
					cathar_female_04					
					cathar_female_05					
					cathar_female_06					
					cathar_female_07					
					cathar_female_08
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					cathar_male_01
					cathar_male_02
					cathar_male_03					
					cathar_male_04
					cathar_male_05
					cathar_male_06
					cathar_male_07
					cathar_male_08
				}
			}
		}

			
		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					cathar_female_01
					cathar_female_02
					cathar_female_03					
					cathar_female_04					
					cathar_female_05					
					cathar_female_06					
					cathar_female_07					
					cathar_female_08
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					cathar_male_01
					cathar_male_02
					cathar_male_03					
					cathar_male_04
					cathar_male_05
					cathar_male_06
					cathar_male_07
					cathar_male_08
				}
			}
		}
	}
}