
portraits = {
	pauan = { entity = "humanoid_05_female_01_entity"	clothes_selector = "humanoid_05_male_clothes_01" hair_selector = "no_texture" greeting_sound = "human_male_greetings_03"
		character_textures = {
			"gfx/models/portraits/pauan/pauan_male_body_01.dds"
			"gfx/models/portraits/pauan/pauan_male_body_02.dds"
			"gfx/models/portraits/pauan/pauan_male_body_03.dds"
			"gfx/models/portraits/pauan/pauan_male_body_04.dds"
		}
	}
}