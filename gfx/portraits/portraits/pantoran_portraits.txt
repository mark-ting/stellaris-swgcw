##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# pantoran
	pantoran_female_01 = { entity = "portrait_pantoran_female_01_entity" clothes_selector = "mammalian_human_female_clothes_01" hair_selector = "pantoran_female_hair_01" greeting_sound = "human_female_greetings_03"
		character_textures = { "gfx/models/portraits/pantoran/pantoran_female_body_01.dds" }
	}
	pantoran_female_02 = { entity = "portrait_pantoran_female_02_entity" clothes_selector = "mammalian_human_female_clothes_01" hair_selector = "pantoran_female_hair_01" greeting_sound = "human_female_greetings_04"
		character_textures = { "gfx/models/portraits/pantoran/pantoran_female_body_02.dds" }
	}
	pantoran_female_03 = { entity = "portrait_pantoran_female_03_entity" clothes_selector = "mammalian_human_female_clothes_01" hair_selector = "pantoran_female_hair_01" greeting_sound = "human_female_greetings_05"
		character_textures = { "gfx/models/portraits/pantoran/pantoran_female_body_02.dds" }
	}
	pantoran_female_04 = { entity = "portrait_pantoran_female_04_entity" clothes_selector = "mammalian_human_female_clothes_01" hair_selector = "pantoran_female_hair_01" greeting_sound = "human_female_greetings_01"
		character_textures = { "gfx/models/portraits/pantoran/pantoran_female_body_02.dds" }
	}
	pantoran_male_01 = { entity = "portrait_pantoran_male_01_entity" clothes_selector = "mammalian_human_male_clothes_01" hair_selector = "pantoran_male_hair_01" greeting_sound = "human_male_greetings_03"
		character_textures = { "gfx/models/portraits/pantoran/pantoran_male_body_01.dds" }
	}	
	pantoran_male_02 = { entity = "portrait_pantoran_male_02_entity" clothes_selector = "mammalian_human_male_clothes_01" hair_selector = "pantoran_male_hair_01" greeting_sound = "human_male_greetings_04"
		character_textures = { "gfx/models/portraits/pantoran/pantoran_male_body_02.dds" }
	}
	pantoran_male_03 = { entity = "portrait_pantoran_male_03_entity" clothes_selector = "mammalian_human_male_clothes_01" hair_selector = "pantoran_male_hair_01" greeting_sound = "human_male_greetings_05"
		character_textures = { "gfx/models/portraits/pantoran/pantoran_male_body_03.dds" }
	}
	pantoran_male_04 = { entity = "portrait_pantoran_male_04_entity" clothes_selector = "mammalian_human_male_clothes_01" hair_selector = "pantoran_male_hair_01" greeting_sound = "human_male_greetings_01"
		character_textures = { "gfx/models/portraits/pantoran/pantoran_male_body_04.dds" }
	}
}

portrait_groups = {
	pantoran = {
		default = pantoran_female_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				trigger = {
					ruler = { gender = male }
				}
				portraits = {
					pantoran_male_01
					pantoran_male_02
					pantoran_male_03
					pantoran_male_04
				}
			}
			add = {
				trigger = {
					ruler = { gender = female }
				}
				portraits = {
					pantoran_female_01
					pantoran_female_02
					pantoran_female_03
					pantoran_female_04
				}
			}
			#set = {
			#	trigger = { ... }
			#	portraits = { ... }
			#	#using "set =" instead of "add" will first clear any portraits already added
			#}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					pantoran_female_01
					pantoran_female_02
					pantoran_female_03
					pantoran_female_04
					pantoran_male_01
					pantoran_male_02
					pantoran_male_03					
					pantoran_male_04
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					pantoran_female_01
					pantoran_female_02
					pantoran_female_03
					pantoran_female_04	
					pantoran_male_01
					pantoran_male_02
					pantoran_male_03					
					pantoran_male_04
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					pantoran_female_01
					pantoran_female_02
					pantoran_female_03
					pantoran_female_04
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					pantoran_male_01
					pantoran_male_02
					pantoran_male_03					
					pantoran_male_04
				}
			}
		}

			
		#leader scope 
		ruler = {
			add = {
				trigger = {
					gender = female
				}
				portraits = {
					pantoran_female_01
					pantoran_female_02
					pantoran_female_03
					pantoran_female_04
				}
			}
			add = {
				trigger = {
					gender = male
				}
				portraits = {
					pantoran_male_01
					pantoran_male_02
					pantoran_male_03					
					pantoran_male_04
				}
			}
		}
	}
}