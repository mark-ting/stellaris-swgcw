##############################################################
###	PORTRAITS SETTINGS
###
### Which portraits are used is set in 
### common\species_classes\00_species_classes.txt
###
###	This file configures how portraits and planet backgrounds are built.
###	Default position orientation is lower left.
##############################################################

portraits = {
	# duro
	duro_male_01 = { entity = "portrait_duro_male_01_entity" clothes_selector = "molluscoid_slender_clothes_01" hair_selector = "no_texture" greeting_sound = "human_male_greetings_03"
		character_textures = { "gfx/models/portraits/duro/duro_male_body_01.dds" }
	}	
	duro_male_02 = { entity = "portrait_duro_male_02_entity" clothes_selector = "molluscoid_slender_clothes_01" hair_selector = "no_texture" greeting_sound = "human_male_greetings_04"
		character_textures = { "gfx/models/portraits/duro/duro_male_body_02.dds" }
	}
	duro_male_03 = { entity = "portrait_duro_male_03_entity" clothes_selector = "molluscoid_slender_clothes_01" hair_selector = "no_texture" greeting_sound = "human_male_greetings_05"
		character_textures = { "gfx/models/portraits/duro/duro_male_body_03.dds" }
	}		
	duro_male_04 = { entity = "portrait_duro_male_04_entity" clothes_selector = "molluscoid_slender_clothes_01" hair_selector = "no_texture" greeting_sound = "human_male_greetings_01"
		character_textures = { "gfx/models/portraits/duro/duro_male_body_04.dds" }
	}
}

portrait_groups = {
	duro = {
		default = duro_male_01
		game_setup = { #will run with a limited country scope. species and government is set but the country does not actually exist
			add = {
				portraits = {
					duro_male_01
					duro_male_02
					duro_male_03					
					duro_male_04
				}
			}
			#set = {
			#	trigger = { ... }
			#	portraits = { ... }
			#	#using "set =" instead of "add" will first clear any portraits already added
			#}
		}		
		
		#species scope
		species = { #generic portrait for a species
			add = {
				portraits = {
					duro_male_01
					duro_male_02
					duro_male_03					
					duro_male_04
				}
			}
		}		
		
		#pop scope
		pop = { #for a specific pop
			add = {
				portraits = {
					duro_male_01
					duro_male_02
					duro_male_03					
					duro_male_04
				}
			}
		}
		
		#leader scope
		leader = { #scientists, generals, admirals, governor
			add = {
				portraits = {
					duro_male_01
					duro_male_02
					duro_male_03					
					duro_male_04
				}
			}
		}

			
		#leader scope 
		ruler = {
			add = {
				portraits = {
					duro_male_01
					duro_male_02
					duro_male_03					
					duro_male_04
				}
			}
		}
	}
}